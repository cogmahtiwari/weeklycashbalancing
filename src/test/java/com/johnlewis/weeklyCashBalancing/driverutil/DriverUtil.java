package com.johnlewis.weeklyCashBalancing.driverutil;

import io.github.bonigarcia.wdm.WebDriverManager;
import net.thucydides.core.webdriver.DriverSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class DriverUtil implements DriverSource {
    @Override
    public WebDriver newDriver() {

        System.setProperty("proxyHost", "jlp-proxy");
        System.setProperty("proxyPort", "80");
//        WebDriverManager.chromedriver().version("74.0.3729.6");
        WebDriverManager.chromedriver().setup();
        return new ChromeDriver();
    }

    @Override
    public boolean takesScreenshots() {
        return true;
    }
}
