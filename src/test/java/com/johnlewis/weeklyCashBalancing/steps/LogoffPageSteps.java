package com.johnlewis.weeklyCashBalancing.steps;

import com.johnlewis.weeklyCashBalancing.pageobjects.LogoffPage;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.steps.ScenarioSteps;

import java.util.List;

public class LogoffPageSteps extends ScenarioSteps {
    @Steps
    LogoffPage logoff;

    public void clickHelpLink(String pageTitle) {
        logoff.clickHelpLink(pageTitle);
    }

    public void menuCTRLClick(List<String> pageTitle) {
        logoff.menuCRTLClick(pageTitle);
    }

    public void clickLogOffLink() {
        logoff.clickLogOffLink();
    }
}
