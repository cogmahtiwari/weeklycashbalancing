package com.johnlewis.weeklyCashBalancing.steps;

import com.johnlewis.weeklyCashBalancing.pageobjects.EnterCashMovementPage;
import net.thucydides.core.annotations.Steps;

import java.util.List;

public class EnterCashMovementPageStep {

    @Steps
    EnterCashMovementPage enterCashMovementPage;

    public void clickEnterCashMovement() {
        enterCashMovementPage.clickEnterCashMovement();
    }

    public void checkForEditableFields() {
        enterCashMovementPage.checkForEditableFields();
    }

    public void movementStatus() {
        enterCashMovementPage.movementStatus();
    }

    public void enterTradingDate(List<String> days) {
        enterCashMovementPage.enterTradingDate(days);
    }

    public void dayValidation(List<String> days) {
        enterCashMovementPage.dayValidation(days);
    }

    public void performActivity(List<String> activity) {
        enterCashMovementPage.performActivity(activity);
    }

    public void validateMovement() {
        enterCashMovementPage.validateMovement();
    }

    public void ValidateMovementValue() {
        enterCashMovementPage.ValidateMovementValue();
    }
}
