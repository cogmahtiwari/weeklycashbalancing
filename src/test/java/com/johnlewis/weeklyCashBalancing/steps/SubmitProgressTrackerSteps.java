package com.johnlewis.weeklyCashBalancing.steps;

import com.johnlewis.weeklyCashBalancing.pageobjects.SubmitProgressTrackerPage;
import net.thucydides.core.annotations.Steps;

import java.util.List;

public class SubmitProgressTrackerSteps {

    @Steps
    SubmitProgressTrackerPage submitProgressTrackerPage;

    public void clickSubmitProgressTracker() {
        submitProgressTrackerPage.clickSubmitProgressTracker();
    }

    public void viewProgressTrackerDetails(List<String> week) {
        submitProgressTrackerPage.viewProgressTrackerDetails(week);
    }

    public void checkPassBackStatus() {
        submitProgressTrackerPage.checkPassBackStatus();
    }

    public void submitPassBack() {
        submitProgressTrackerPage.submitPassBack();
    }
}
