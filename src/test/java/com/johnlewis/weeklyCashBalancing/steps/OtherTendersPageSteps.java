package com.johnlewis.weeklyCashBalancing.steps;

import com.johnlewis.weeklyCashBalancing.pageobjects.OtherTendersPage;
import net.thucydides.core.annotations.Steps;

public class OtherTendersPageSteps {

    @Steps
    OtherTendersPage otherTendersPage;

    public void clickOtherTenders() {
        otherTendersPage.clickOtherTenders();
    }

    public void enterValueInTenders() {
        otherTendersPage.enterValueInTenders();
    }

    public void sectionS00AndS99Protected() {
        otherTendersPage.sectionS00AndS99Protected();
    }

    public void enterTradeDate(String date) {
        otherTendersPage.enterTradeDate(date);
    }

    public void checkFieldStatus() {
        otherTendersPage.checkFieldStatus();
    }
}
