package com.johnlewis.weeklyCashBalancing.steps;

import com.johnlewis.weeklyCashBalancing.pageobjects.EnterDailyWCBTotalPage;
import net.thucydides.core.annotations.Steps;

import java.util.List;

public class EnterDailyWCBTotalPageSteps {
    @Steps
    EnterDailyWCBTotalPage dailyWCBTotalPage;

    public void clickEnterDailyWCBTotal() {
        dailyWCBTotalPage.clickEnterDailyWCBTotal();
    }

    public void enterTradingDate(List<String> day) {
        dailyWCBTotalPage.enterTradingDate(day);
    }

    public void errorMessageDisplay() {
        dailyWCBTotalPage.errorMessageDisplay();
    }

    public void clickHomeButton() {
        dailyWCBTotalPage.clickHomeButton();
    }

    public void acceptAlertMessage() {
        dailyWCBTotalPage.acceptAlertMessage();
    }

    public void closeFloatAndReserveStatus() {
        dailyWCBTotalPage.closeFloatAndReserveStatus();
    }

    public void sortSection() {
        dailyWCBTotalPage.sortSection();
    }

    public void selectSections() {
        dailyWCBTotalPage.selectSection();
    }

    public void enterFloatAtRegisterLevel() {
        dailyWCBTotalPage.enterFloatAtRegisterLevel();
    }

    public void enterFloatAtSectionLevel() {
        dailyWCBTotalPage.enterFloatAtSectionLevel();
    }

    public void enterValueInRegister() {
        dailyWCBTotalPage.enterValueInRegister();
    }

    public void verifyBusinessRule() {
        dailyWCBTotalPage.verifyBusinessRule();
    }

    public void enterValueInAdjustment() {
//        dailyWCBTotalPage.enterValueInAdjustment();
    }

    public void verifyAdjustmentReason() {
//        dailyWCBTotalPage.verifyAdjustmentReason();
    }

    public void cancelUserInput() {
        dailyWCBTotalPage.cancelUserInput();
    }

    public void clickS00AndS99Sections() {
        dailyWCBTotalPage.clickS00AndS99Sections();
    }

    public void sooAndS99ClosingFloatAndReserveValidation() {
        dailyWCBTotalPage.sooAndS99ClosingFloatAndReserveValidation();
    }

    public void verifyS99Adjustment() {
//        dailyWCBTotalPage.verifyS99Adjustment();
    }

    public void selectSubmittedDate() {
        dailyWCBTotalPage.selectSubmittedDate();
    }

    public void checkForEditableFields() {
        dailyWCBTotalPage.checkForEditableFields();
    }


    public void verifyMovementColumn() {
        dailyWCBTotalPage.verifyMovementColumn();
    }
}
