package com.johnlewis.weeklyCashBalancing.steps;

import com.johnlewis.weeklyCashBalancing.pageobjects.ViewAuditTrailPage;
import net.thucydides.core.annotations.Steps;

public class ViewAuditTrailSteps {

    @Steps
    ViewAuditTrailPage viewAuditTrailPage;

    public void clickViewAuditTrail() {
        viewAuditTrailPage.clickViewAuditTrail();
    }

    public void displayAuditTrail() {
        viewAuditTrailPage.displayAuditTrail();
    }

    public void printAllBranchAuditTrailReport() {
        viewAuditTrailPage.printAllBranchAuditTrailReport();
    }
}
