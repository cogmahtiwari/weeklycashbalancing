package com.johnlewis.weeklyCashBalancing.steps;

import com.johnlewis.weeklyCashBalancing.pageobjects.MaintainRegisterAndSectionPage;
import net.thucydides.core.annotations.Steps;

public class MaintainRegisterAndSectionSteps {

    @Steps
    MaintainRegisterAndSectionPage maintainRegisterAndSectionPage;


    public void clickMaintainRegisterAndSection() {
        maintainRegisterAndSectionPage.clickMaintainRegisterAndSection();
    }

    public void sortedSectionsList() {
        maintainRegisterAndSectionPage.sortedSectionsList();
    }

    public void clickSections() {
        maintainRegisterAndSectionPage.clickSections();
    }

    public void editSectionsAndRegister() {
        maintainRegisterAndSectionPage.editSectionsAndRegister();
    }

    public void addNewRegister(String regNumber, String sectionNumber, String description) {
        maintainRegisterAndSectionPage.addNewRegister(regNumber, sectionNumber, description);
    }

    public void addNewSection(String sectionNumber, String description) {
        maintainRegisterAndSectionPage.addNewSection(sectionNumber, description);
    }

    public void saveDetails() {
        maintainRegisterAndSectionPage.saveDetails();
    }

    public void clickListAllRegisters() {
        maintainRegisterAndSectionPage.clickListAllRegisters();
    }
}
