package com.johnlewis.weeklyCashBalancing.steps;

import com.johnlewis.weeklyCashBalancing.pageobjects.HomePage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class HomePageSteps extends ScenarioSteps {

    HomePage homepageOpenURL;
    @Step
    public void openURL() {
        homepageOpenURL.openAt("http://tbwasb01:9330/dsm_drs_WklyCashBalance/logon.do");
    }

    public void signInToMyAccountSteps(String uid, String password) {
        homepageOpenURL.signInToMyAccount(uid, password);
    }

    public void selectBranch(String branchName) {
        homepageOpenURL.selectBranch(branchName);
    }

    public void verifyTitle(String pageTitle) {
        homepageOpenURL.verifyTitle(pageTitle);
    }

    public void getBranchName() {
        homepageOpenURL.getBranchName();
    }
}
