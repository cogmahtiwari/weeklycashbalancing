package com.johnlewis.weeklyCashBalancing.steps;

import com.johnlewis.weeklyCashBalancing.pageobjects.ViewandSubmitWeeklyTotalsPage;
import net.thucydides.core.annotations.Steps;

import java.util.List;

public class ViewandSubmitWeeklyTotalsSteps {

    @Steps
    ViewandSubmitWeeklyTotalsPage viewAndSubmitWeeklyTotalsPage;

    public void clickviewAndSubmitWeeklyTotals() {
        viewAndSubmitWeeklyTotalsPage.clickviewAndSubmitWeeklyTotals();
    }

    public void enterTradingWeek(List<String> week) {
        viewAndSubmitWeeklyTotalsPage.enterTradingWeek(week);
    }

    public void submitWeeklyTotals() {
        viewAndSubmitWeeklyTotalsPage.submitWeeklyTotals();
    }
}
