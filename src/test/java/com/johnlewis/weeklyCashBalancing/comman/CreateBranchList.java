package com.johnlewis.weeklyCashBalancing.comman;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.LinkedHashSet;

public class CreateBranchList {

    public void writeBranchList(String branchName) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("./Output/Reports/BranchList.txt", true));
            writer.write(branchName);
            writer.newLine();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public LinkedHashSet<String> readBranchList() {
        LinkedHashSet<String> branchList = new LinkedHashSet<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader("./Output/Reports/BranchList.txt"));
            String branchName;
            while ((branchName = reader.readLine()) != null) {
                branchList.add(branchName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return branchList;
    }
}
