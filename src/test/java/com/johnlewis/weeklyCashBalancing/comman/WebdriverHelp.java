package com.johnlewis.weeklyCashBalancing.comman;

import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.NoSuchElementException;

public class WebdriverHelp {

    public WebElement getWebElement(WebElementFacade element) {
        try {
            return element;
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    public void AlertMessage(WebDriver driver, String action) {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.alertIsPresent());
        Alert messageAlert = driver.switchTo().alert();
        System.out.println("Alert Message on screen :" + messageAlert.getText());
        if (action.toLowerCase().equalsIgnoreCase("accept"))
            messageAlert.accept();
        else
            messageAlert.dismiss();
    }

    public boolean isAlertPresent(WebDriver driver) {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException Ex) {
            return false;
        }
    }

    public String getRequiredDate(String days) {
        Calendar calendar = Calendar.getInstance();
        DateFormat formattedDate = new SimpleDateFormat("dd-MM-yyyy");
        String day = days.toLowerCase();
        switch (day) {
            case "today's":
                formattedDate.format(calendar.getTime());
                break;
            case "future":
                calendar.add(Calendar.DATE, 7);
                break;
            case "saturday":
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("u");
                String date1 = simpleDateFormat.format(calendar.getTime());
                int dayNumber = Integer.parseInt(date1);
                int dayValue;
                if (dayNumber < 6) {
                    calendar.add(Calendar.DATE, -7);
                    dayValue = (6 - dayNumber);
                    calendar.add(Calendar.DATE, dayValue);
                } else if (dayNumber > 6)
                    calendar.add(Calendar.DATE, -1);
                break;

            default:
                calendar.add(Calendar.DATE, -1);
                break;
        }
        return formattedDate.format(calendar.getTime());
    }

    public String getRequiredSat(String days, String setDate) {
        Calendar calendar = Calendar.getInstance();
        DateFormat formattedDate = new SimpleDateFormat("dd-MM-yyyy");
        String day = days.toLowerCase();
        switch (day) {
            case "again saturday":
                try {
                    calendar.setTime(formattedDate.parse(setDate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                calendar.add(Calendar.DATE, -7);
                break;
        }
        return formattedDate.format(calendar.getTime());
    }

    public String getRequiredWeek(String week) {
        Calendar calender = Calendar.getInstance();
        DateFormat formatedDate = new SimpleDateFormat("dd.MM.yyyy");
        String day = week.toLowerCase();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("u");
        String date1 = simpleDateFormat.format(calender.getTime());
        int dayNumber = Integer.parseInt(date1);
        int dayValue;

        switch (day) {
            case "current":
                if (dayNumber < 6) {
                    calender.add(Calendar.DATE, -7);
                    dayValue = (6 - dayNumber);
                    calender.add(Calendar.DATE, dayValue);
                } else if (dayNumber > 6)
                    calender.add(Calendar.DATE, -1);
                else
                    calender.add(Calendar.DATE, -7);
                break;
            case "past":
                if (dayNumber < 6) {
                    calender.add(Calendar.DATE, -14);
                    dayValue = (6 - dayNumber);
                    calender.add(Calendar.DATE, dayValue);
                } else if (dayNumber > 6)
                    calender.add(Calendar.DATE, -1);
                else
                    calender.add(Calendar.DATE, -7);
                break;
            default:
                if (dayNumber < 6) {
                    dayValue = (6 - dayNumber);
                    calender.add(Calendar.DATE, dayValue);
                } else if (dayNumber > 6) {
                    calender.add(Calendar.DATE, 6);
                }
                break;
        }
        return formatedDate.format(calender.getTime());
    }

    public void jsClick(WebDriver driver, WebElementFacade element) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click();", element);
    }

    /*public WebElement waitForElement(WebDriver driver, By by) {

        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
        wait.pollingEvery(1, TimeUnit.SECONDS);
        wait.withTimeout(3000, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);

        WebElement foo = wait.until(new Function<WebDriver, WebElement>() {

            public WebElement apply(WebDriver driver) {
                System.out.println("Inside Wait method");
                WebElement element = driver.findElement(by);
                return element;
            }
        });
        return foo;
    }*/
}
