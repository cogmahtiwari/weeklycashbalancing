package com.johnlewis.weeklyCashBalancing.stepdefs;

import com.johnlewis.weeklyCashBalancing.steps.HomePageSteps;
import com.johnlewis.weeklyCashBalancing.steps.SubmitProgressTrackerSteps;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

import java.util.List;

public class SubmitProgressTrackerDefination {

    @Steps
    SubmitProgressTrackerSteps submitProgressTrackerSteps;

    @Steps
    HomePageSteps homePageSteps;

    @Given("^user able to open Submit Progress Tracker$")
    public void userAbleToOpenSubmitProgressTracker() {
        homePageSteps.getBranchName();
        submitProgressTrackerSteps.clickSubmitProgressTracker();
    }

    @When("^user select the (.*) as trading week and tracker details should display$")
    public void userSelectTheWeekAsTradingWeekAndTrackerDetailsShouldDisplay(List<String> week) {
        submitProgressTrackerSteps.viewProgressTrackerDetails(week);
    }

    @And("^user click on pass back to resubmit weekly totals$")
    public void userClickOnPassBackToDoResubmit() {
        submitProgressTrackerSteps.clickSubmitProgressTracker();
        submitProgressTrackerSteps.submitPassBack();
    }

    @Then("^pass back only enabled for branches that have submitted their week$")
    public void passbackOnlyEnabledForBranchesThatHaveSubmittedTheirWeek() {
        submitProgressTrackerSteps.clickSubmitProgressTracker();
        submitProgressTrackerSteps.checkPassBackStatus();
    }


}
