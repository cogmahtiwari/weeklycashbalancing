package com.johnlewis.weeklyCashBalancing.stepdefs;

import com.johnlewis.weeklyCashBalancing.steps.EnterCashMovementPageStep;
import com.johnlewis.weeklyCashBalancing.steps.HomePageSteps;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

import java.util.List;

public class EnterCashMovementStepDefination {

    @Steps
    EnterCashMovementPageStep enterCashMovementPageStep;

    @Steps
    HomePageSteps homePageSteps;

    @Given("^User able to open Enter Cash Movements$")
    public void userAbleToOpenEnterCashMovements() {
        homePageSteps.getBranchName();
        homePageSteps.selectBranch("Peter Jones");
        enterCashMovementPageStep.clickEnterCashMovement();
    }

    @When("^user enter the (.*) as cash movement trading date$")
    public void userEnterTheDateAsCashMovementTradingDate(List<String> days) {
        enterCashMovementPageStep.enterTradingDate(days);
    }

    @Then("^date out of range message should be displayed for future (.*) only$")
    public void dateOutOfRangeMessageShouldBeDisplayedForFutureDateOnly(List<String> days) {
        enterCashMovementPageStep.dayValidation(days);
    }

    @And("^user enter cash movement details, user allowed to enter value as per business rule$")
    public void userEnterCashMovementDetailsUserAllowedToEnterValueAsPerBusinessRule() {
        enterCashMovementPageStep.validateMovement();
        enterCashMovementPageStep.ValidateMovementValue();
    }

    @And("^There should be no editable fields & a message of 'This Week Totals Submitted'$")
    public void thereShouldBeNoEditableFieldsAMessageOfThisWeekTotalsSubmitted() {
        enterCashMovementPageStep.checkForEditableFields();
        enterCashMovementPageStep.movementStatus();
    }

    @Then("^user do (.*) after entries have been keyed for cash movement then Warning message should display$")
    public void userDoActivityAfterEntriesHaveBeenKeyedForCashMovementThenWarningMessageShouldDisplay(List<String> activity) {
        enterCashMovementPageStep.performActivity(activity);
    }


}
