package com.johnlewis.weeklyCashBalancing.stepdefs;

import com.johnlewis.weeklyCashBalancing.steps.HomePageSteps;
import com.johnlewis.weeklyCashBalancing.steps.ViewAuditTrailSteps;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class ViewAuditTrailDefination {

    @Steps
    ViewAuditTrailSteps viewAuditTrailSteps;

    @Steps
    HomePageSteps homePageSteps;

    @Given("^User able to open View Audit Trail$")
    public void userAbleToOpenViewAuditTrail() {
        homePageSteps.getBranchName();
        viewAuditTrailSteps.clickViewAuditTrail();
    }

    @When("^user select the week as trading week and sorted audit details should display$")
    public void userSelectTheWeekAsTradingWeekAndAuditDetailsShouldDisplay() {
        viewAuditTrailSteps.displayAuditTrail();
    }

    @And("^user change to another authorised branch to \"([^\"]*)\"$")
    public void userChangeToAnotherAuthorisedBranch(String branch) {
        homePageSteps.selectBranch(branch);
    }

    @Then("^display the audit report of all branch$")
    public void displayTheAuditReportOfAllBranch() {
        viewAuditTrailSteps.printAllBranchAuditTrailReport();
    }
}
