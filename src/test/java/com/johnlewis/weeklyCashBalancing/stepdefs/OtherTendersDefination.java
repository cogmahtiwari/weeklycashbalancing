package com.johnlewis.weeklyCashBalancing.stepdefs;

import com.johnlewis.weeklyCashBalancing.steps.EnterDailyWCBTotalPageSteps;
import com.johnlewis.weeklyCashBalancing.steps.HomePageSteps;
import com.johnlewis.weeklyCashBalancing.steps.MaintainRegisterAndSectionSteps;
import com.johnlewis.weeklyCashBalancing.steps.OtherTendersPageSteps;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

import java.util.Arrays;

public class OtherTendersDefination {

    @Steps
    OtherTendersPageSteps otherTendersPage;

    @Steps
    MaintainRegisterAndSectionSteps maintainRegisterAndSectionPage;

    @Steps
    EnterDailyWCBTotalPageSteps dailyWCBTotalPageSteps;

    @Steps
    HomePageSteps homePageSteps;

    @Given("^user on the Enter Other Tenders$")
    public void userOnTheEnterOtherTenders() {
        otherTendersPage.clickOtherTenders();
    }

    @When("^user sort section by name and code and enter float in section as as per business rule$")
    public void userSortSectionByNameAndCodeAndEnterFloatInSectionAsAsPerBusinessRule() {
        dailyWCBTotalPageSteps.enterTradingDate(Arrays.asList(""));
        maintainRegisterAndSectionPage.sortedSectionsList();
        dailyWCBTotalPageSteps.sortSection();
        maintainRegisterAndSectionPage.sortedSectionsList();
    }

    @Then("^section S00 and S99 should protected and value accepted as per business rule$")
    public void sectionSAndSShouldProtectedAndValueAcceptedAsPerBusinessRule() {
        otherTendersPage.sectionS00AndS99Protected();
        otherTendersPage.enterValueInTenders();
    }

    @When("^user change date and branch before saving changes the warning message should display$")
    public void userChangeDateAndBranchBeforeSavingChangesTheWarningMessageShouldDisplay() {
        otherTendersPage.enterValueInTenders();
        dailyWCBTotalPageSteps.enterTradingDate(Arrays.asList("wednesday"));
        otherTendersPage.enterValueInTenders();
        homePageSteps.selectBranch("Oxford Street");
    }

    @And("^user click on cancel button then warning message should display$")
    public void userClickOnCancelButtonWithNwarningMessageShouldDisplay() {
        dailyWCBTotalPageSteps.cancelUserInput();
    }


    @And("^user click on Home before saving changes then warning message should display$")
    public void userClickOnHomeBeforeSavingChangesThenWarningMessageShouldDisplay() {
        otherTendersPage.enterValueInTenders();
        dailyWCBTotalPageSteps.clickHomeButton();
    }

    @And("^user Select a \"([^\"]*)\" from a week that has been submitted already$")
    public void userSelectAFromAWeekThatHasBeenSubmittedAlready(String date) {
        otherTendersPage.enterTradeDate(date);
    }

    @Then("^There should be no editable fields$")
    public void thereShouldBeNoEditableFields() {
        otherTendersPage.checkFieldStatus();
    }

}
