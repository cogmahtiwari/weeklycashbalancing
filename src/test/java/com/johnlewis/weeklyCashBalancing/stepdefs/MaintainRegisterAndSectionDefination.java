package com.johnlewis.weeklyCashBalancing.stepdefs;

import com.johnlewis.weeklyCashBalancing.steps.EnterDailyWCBTotalPageSteps;
import com.johnlewis.weeklyCashBalancing.steps.HomePageSteps;
import com.johnlewis.weeklyCashBalancing.steps.MaintainRegisterAndSectionSteps;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class MaintainRegisterAndSectionDefination {

    @Steps
    MaintainRegisterAndSectionSteps maintainRegisterAndSectionSteps;

    @Steps
    EnterDailyWCBTotalPageSteps dailyWCBTotalPageSteps;

    @Steps
    HomePageSteps homePageSteps;

    @Given("^user on Maintain Registers and Sections screen$")
    public void userOnMaintainRegistersAndSectionsScreen() {
        homePageSteps.getBranchName();
        maintainRegisterAndSectionSteps.clickMaintainRegisterAndSection();
    }

    @When("^user sort section by name and code then section display accordingly$")
    public void userSortSectionByNameAndCodeThenSectionDisplayAccordingly() {
        maintainRegisterAndSectionSteps.sortedSectionsList();
        dailyWCBTotalPageSteps.sortSection();
        maintainRegisterAndSectionSteps.sortedSectionsList();
    }

    @And("^user click on sections then available section should display$")
    public void userClickOnSectionsThenAvilableSectionShouldDisplay() {
        dailyWCBTotalPageSteps.sortSection();
        maintainRegisterAndSectionSteps.clickSections();//also have list of register not assigned to any section
    }

    @Then("^user click on edit details then register and section validation should be as per business rule$")
    public void userClickOnEditSectionThenSectionValidationShouldBeAsPerBusinessRule() {
        maintainRegisterAndSectionSteps.clickMaintainRegisterAndSection();
        maintainRegisterAndSectionSteps.editSectionsAndRegister();
        maintainRegisterAndSectionSteps.clickListAllRegisters();
    }

    @When("^user add section \"([^\"]*)\" with description \"([^\"]*)\"$")
    public void userAddSectionWithDescriptionThenSectionShouldBeAdded(String sectionNumber, String description) {
        maintainRegisterAndSectionSteps.addNewSection(sectionNumber, description);
    }

    @When("^user add register \"([^\"]*)\" with description \"([^\"]*)\" in section \"([^\"]*)\"$")
    public void userAddSectionWithDescription(String regNumber, String sectionNumber, String description) {
        maintainRegisterAndSectionSteps.addNewRegister(regNumber, sectionNumber, description);
    }

    @Then("^details should be added$")
    public void detailsShouldBeAdded() {
        maintainRegisterAndSectionSteps.saveDetails();
    }

}
