package com.johnlewis.weeklyCashBalancing.stepdefs;

import com.johnlewis.weeklyCashBalancing.steps.EnterDailyWCBTotalPageSteps;
import com.johnlewis.weeklyCashBalancing.steps.HomePageSteps;
import com.johnlewis.weeklyCashBalancing.steps.MaintainRegisterAndSectionSteps;
import com.johnlewis.weeklyCashBalancing.steps.ViewandSubmitWeeklyTotalsSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

import java.util.List;

public class ViewandSubmitWeeklyTotalsDefination {

    @Steps
    ViewandSubmitWeeklyTotalsSteps viewAndSubmitWeeklyTotalsSteps;

    @Steps
    HomePageSteps homePageSteps;

    @Steps
    MaintainRegisterAndSectionSteps maintainRegisterAndSectionSteps;

    @Steps
    EnterDailyWCBTotalPageSteps dailyWCBTotalPageSteps;

    @Given("^user on the View and Submit Weekly Totals$")
    public void userOnTheViewAndSubmitWeeklyTotals() {
        homePageSteps.getBranchName();
        viewAndSubmitWeeklyTotalsSteps.clickviewAndSubmitWeeklyTotals();
    }

    @When("^user select the (.*) as trading week and details should display$")
    public void userEnterTheWeekAsTradingWeek(List<String> week) {
        viewAndSubmitWeeklyTotalsSteps.enterTradingWeek(week);
    }

    @Then("^Change to another authorised branch before and after submitting$")
    public void changeToAnotherAuthorisedBranchBeforeAndAfterSubmitting() {
        homePageSteps.selectBranch("Peter Jones");
    }

    @When("^user sort section by name and code list should display accordingly$")
    public void userSortSectionByNameAndCodeAndUserSubmitTheDataForWeek() {
        viewAndSubmitWeeklyTotalsSteps.clickviewAndSubmitWeeklyTotals();
        maintainRegisterAndSectionSteps.sortedSectionsList();
        dailyWCBTotalPageSteps.sortSection();
        maintainRegisterAndSectionSteps.sortedSectionsList();
    }

    @Then("^user submit for week then warning message should display before submitting$")
    public void userSubmitForWeekThenWarningMessageShouldDisplayBeforeSubmitting() {
        viewAndSubmitWeeklyTotalsSteps.submitWeeklyTotals();
    }
}
