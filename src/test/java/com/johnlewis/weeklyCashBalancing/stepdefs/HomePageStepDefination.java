package com.johnlewis.weeklyCashBalancing.stepdefs;

import com.johnlewis.weeklyCashBalancing.steps.HomePageSteps;
import com.johnlewis.weeklyCashBalancing.steps.LogoffPageSteps;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.steps.StepEventBus;
import uk.org.lidalia.sysoutslf4j.context.SysOutOverSLF4J;

import java.util.List;

public class HomePageStepDefination {

    static {
        SysOutOverSLF4J.sendSystemOutAndErrToSLF4J();
    }

    @Steps
    HomePageSteps homePageSteps;

    @Steps
    LogoffPageSteps logoffSteps;

    @Given("^User opens the WCB Application url to login$")
    public void userOpenURL() {
        homePageSteps.openURL();
    }

    @When("^User login to WCB Application using  valid userName \"([^\"]*)\" and password \"([^\"]*)\" and click logon button$")
    public void userLoginToWCBApplicationUsingValidAnd(String uId, String password) {
        homePageSteps.signInToMyAccountSteps(uId, password);
    }

    @Then("^User able to select \"([^\"]*)\"$")
    public void userAbleToSelectBrach(String branchName) {
        homePageSteps.selectBranch(branchName);
    }

    @Then("^verify the title of page is \"([^\"]*)\"$")
    public void verifyTitalOfPage(String pageTitle) {
        homePageSteps.verifyTitle(pageTitle);
    }

    @Given("^User on homepage of weekly cash balancing and changes branch to \"([^\"]*)\"$")
    public void userOnHomepageOfWeeklyCashBalancingAndChangesBranchTo(String branchName) {
        homePageSteps.selectBranch(branchName);
    }

    @When("^user click on help link verify the title of page is \"([^\"]*)\"$")
    public void userClickOnHelpLink(String pageTitle) {
        logoffSteps.clickHelpLink(pageTitle);
    }

    @And("^user press CLTR and click on to open the menu in multiple tab and Verify tital of page \\\"([^\\\"]*)\\\"$")
    public void userCLTRAndClickToOpenTheMenuInMultipleTab(List<String> pageTitle) {
        logoffSteps.menuCTRLClick(pageTitle);

    }

    @Then("^user logoff from WCB Application$")
    public void userLogoffFromWCBApplication() {
        logoffSteps.clickLogOffLink();
    }
}
