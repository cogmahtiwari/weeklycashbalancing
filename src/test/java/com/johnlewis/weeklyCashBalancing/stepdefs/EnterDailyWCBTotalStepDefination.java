package com.johnlewis.weeklyCashBalancing.stepdefs;

import com.johnlewis.weeklyCashBalancing.steps.EnterDailyWCBTotalPageSteps;
import com.johnlewis.weeklyCashBalancing.steps.HomePageSteps;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

import java.util.Arrays;
import java.util.List;

public class EnterDailyWCBTotalStepDefination {

    @Steps
    EnterDailyWCBTotalPageSteps dailyWCBTotalPageSteps;

    @Steps
    HomePageSteps homePageSteps;

    @Given("^user on the Enter Daily WCB totals Screen$")
    public void userOnTheEnterDailyWCBTotalsScreen() {
        homePageSteps.getBranchName();
        dailyWCBTotalPageSteps.clickEnterDailyWCBTotal();
    }

    @When("^user enter the (.*) as date$")
    public void userEnterTheTodaySOrFutureDate(List<String> day) {
        dailyWCBTotalPageSteps.enterTradingDate(day);
    }

    @Then("^date out of range message should be displayed$")
    public void dateRangeIncorrectMessageShouldBeDisplayed() {
        dailyWCBTotalPageSteps.errorMessageDisplay();
    }

    @When("^user select a date that is a \"([^\"]*)\"$")
    public void userSelectADateThatIsASaturday(List<String> day) {
        dailyWCBTotalPageSteps.enterTradingDate(day);
    }

    @Then("^closing Float and closing reserve columns are editable$")
    public void closingFloatAndClosingReserveColumnsAreEditable() {
        dailyWCBTotalPageSteps.closeFloatAndReserveStatus();

    }

    @When("^user change the date with saving details to that is not a saturday$")
    public void userSelectADateThatIsNotASaturday() {
        dailyWCBTotalPageSteps.enterTradingDate(Arrays.asList("wednesday"));
    }

    @Then("^closing float and closing reserve columns are protected and warning message should display$")
    public void closingFloatAndClosingReserveColumnsAreProtected() {
        dailyWCBTotalPageSteps.closeFloatAndReserveStatus();
    }

    @When("^change the branch after entries have been keyed$")
    public void userChangeDateAfterEntriesHaveBeenKeyed() {
        dailyWCBTotalPageSteps.enterTradingDate(Arrays.asList("saturday"));
    }

    @Then("^warning message should display and return to homepage$")
    public void warningMessageShouldDisplay() {
        dailyWCBTotalPageSteps.closeFloatAndReserveStatus();
        homePageSteps.selectBranch("Oxford Street");
    }

    @When("^user select section to enter the floating details then assigned register should display$")
    public void userSelectSectionToEnterTheFloatingDetails() {
        dailyWCBTotalPageSteps.enterTradingDate(Arrays.asList("saturday"));
        dailyWCBTotalPageSteps.sortSection();
        dailyWCBTotalPageSteps.selectSections();
    }

    @And("^user enter float at section level then register level fields get protected$")
    public void userEnterFloatAtSectionLevelThenRegisterLevelFieldsGetProtected() {
        dailyWCBTotalPageSteps.enterFloatAtSectionLevel();
    }


    @And("^user enter float at register level as as per business rule$")
    public void userEnterFloatAtRegisterAsAsPerBusinessRule() {
        dailyWCBTotalPageSteps.enterValueInRegister();
    }

    @And("^section level fields get protected and user allowed to enter value as per business rule$")
    public void sectionLevelFieldsGetProtectedAndUserAllowedToEnterValueAsPerBusinessRule() {
        dailyWCBTotalPageSteps.verifyBusinessRule();
    }

    @Then("^cancel the user input$")
    public void cancelTheUserInput() {
        dailyWCBTotalPageSteps.cancelUserInput();
    }

    /*@When("^user enter Value in OTHER adjustment reason at section level$")
    public void userEnterValueInOTHERAdjustmentReasonAtSectionLevel() {
        dailyWCBTotalPageSteps.enterValueInAdjustment();
    }

    @Then("^register level adjustment should be protected and data get saved$")
    public void registerLevelAdjustmentShouldBeProtectedAndAbleToEnterTheValue() {
        dailyWCBTotalPageSteps.verifyAdjustmentReason();
    }*/

    @When("^user click on section S00 and S99 then S00 register only display$")
    public void userClickOnSectionS00AndS99ThenS00RegisterOnlyDisplay() {
        dailyWCBTotalPageSteps.enterTradingDate(Arrays.asList("saturday"));
        dailyWCBTotalPageSteps.clickS00AndS99Sections();
    }

   /* @And("^section level closing float and closing reserve columns are protected$")
    public void sectionLevelClosingFloatAndClosingReserveColumnsAreProtected() {
        dailyWCBTotalPageSteps.sooAndS99ClosingFloatAndReserveValidation();
    }*/

    @And("^user enter value in adjustment for section S00$")
    public void userEnterValueInAdjustmentForSectionSAndS() {
//        dailyWCBTotalPageSteps.enterValueInAdjustment();
    }

    @Then("^for section S00,S99,SAFE float and change machine Field should be protected$")
    public void forSectionSFieldShouldBeProtected() {
        dailyWCBTotalPageSteps.sooAndS99ClosingFloatAndReserveValidation();
        dailyWCBTotalPageSteps.verifyS99Adjustment();
    }

    @When("^user click on Home before saving changes$")
    public void userClickOnHomeBeforeSavingChanges() {
        dailyWCBTotalPageSteps.clickEnterDailyWCBTotal();
        dailyWCBTotalPageSteps.enterTradingDate(Arrays.asList("saturday"));
        dailyWCBTotalPageSteps.closeFloatAndReserveStatus();
    }

    @Then("^warning message should display$")
    public void warningMessageHouldDisplay() {
        dailyWCBTotalPageSteps.clickHomeButton();
    }

    @When("^user select a date from a week that has been submitted already$")
    public void userSelectADateFromAWeekThatHasBeenSubmittedAlready() {
        dailyWCBTotalPageSteps.selectSubmittedDate();
    }

    @Then("^There should be no editable fields & a message of 'Totals Submitted'$")
    public void thereShouldBeNoEditableFieldsAMessageOfTotalsSubmitted() {
        dailyWCBTotalPageSteps.checkForEditableFields();
        dailyWCBTotalPageSteps.errorMessageDisplay();
    }

    @And("^Verify Movement details are not editable$")
    public void verifyMovementDetailsAreNotEditable() {
        dailyWCBTotalPageSteps.verifyMovementColumn();
    }

    @Then("^for section S00 and S99 Field should be protected$")
    public void forSectionSAndSFieldShouldBeProtected() {
        dailyWCBTotalPageSteps.sooAndS99ClosingFloatAndReserveValidation();
    }
}
