package com.johnlewis.weeklyCashBalancing.pageobjects;

import com.johnlewis.weeklyCashBalancing.comman.WebdriverHelp;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import java.util.LinkedList;
import java.util.List;

public class LogoffPage extends PageObject {

    @FindBy(id = "headerSystemTitle")
    WebElementFacade titleOfPage;

    @FindBy(id = "submit")
    WebElementFacade selectButton;

    @FindBy(xpath = "//div[@id='divHelp']/a")
    WebElementFacade helpLink;

    @FindBy(id = "wcbTotals")
    WebElementFacade wcbTotal;

    @FindBy(id = "otherTenders")
    WebElementFacade otherTenders;

    @FindBy(id = "cashMovement")
    WebElementFacade enterCashMovement;

    @FindBy(id = "viewAndUpload")
    WebElementFacade viewAndSubmit;

    @FindBy(id = "sectionAndRegister")
    WebElementFacade maintainSecAndReg;

    @FindBy(id = "audit")
    WebElementFacade auditTrail;

    @FindBy(xpath = "//img[contains(@id,'Progress')]")
    WebElementFacade submitTracker;

    @FindBy(xpath = "//span[@class='noBreak' and contains(text(),'Log')]")
    WebElementFacade logOffLink;

    @FindBy(xpath = "//span[contains(@class,'noBreak message')]")
    WebElementFacade notificationMessage;

    @FindBy(css = ".welcomeOption a")
    List<WebElementFacade> elementToCrtlClick;

    WebdriverHelp driverHelp = new WebdriverHelp();

    public void clickHelpLink(String pageTitle) {
        helpLink.waitUntilVisible().click();
        System.out.println("User clicked on " + helpLink.getText());
        String oldTab = getDriver().getWindowHandle();
        LinkedList<String> newTab = new LinkedList<String>(getDriver().getWindowHandles());
        for (String winHandel : newTab) {
            getDriver().switchTo().window(winHandel);
            if (pageTitle.equalsIgnoreCase(titleOfPage.getText())) {
                System.out.println("User on \"" + titleOfPage.getText() + "\" Screen");
                getDriver().close();
            } else {
                System.out.println("User currently on \"" + titleOfPage.getText() + "\"");
            }
        }
        getDriver().switchTo().window(oldTab);
    }

    public void menuCRTLClick(List<String> pageTitle) {
        String oldTab = getDriver().getWindowHandle();

        /*LinkedList<WebElement> elementToCrtlClick = new LinkedList<WebElement>();
        elementToCrtlClick.add(wcbTotal);
//        elementToCrtlClick.add(otherTenders);
        elementToCrtlClick.add(enterCashMovement);
        elementToCrtlClick.add(viewAndSubmit);
        elementToCrtlClick.add(maintainSecAndReg);
        elementToCrtlClick.add(auditTrail);
        elementToCrtlClick.add(submitTracker);*/

//        List<WebElement> elementToCrtlClick = getDriver().findElements(By.cssSelector(".welcomeOption a"));
//
//        ((JavascriptExecutor)getDriver()).executeScript("arguments[0].dispatchEvent(new MouseEvent('click', {ctrlKey: true}))",eleme);

        for (WebElement element : elementToCrtlClick) {
            ((JavascriptExecutor) getDriver()).executeScript("arguments[0].dispatchEvent(new MouseEvent('click', {ctrlKey: true}))", element);
            /*Actions action = new Actions(getDriver());
            action.keyDown(Keys.CONTROL);
            action.click(element);
            action.keyUp(Keys.CONTROL);
            action.build().perform();*/
            /*Actions actions = new Actions(getDriver());
            actions.keyDown(Keys.CONTROL).moveToElement(element).click(); //press control key
            actions.keyUp(Keys.CONTROL).build().perform(); //release control key*/
        }
        LinkedList<String> newTab = new LinkedList<String>(getDriver().getWindowHandles());
        for (String winHandle : newTab) {
            getDriver().switchTo().window(winHandle);
            if (pageTitle.contains(titleOfPage.getText())) {
                System.out.println("User on \"" + titleOfPage.getText() + "\" Screen");
                getDriver().close();
            } else {
                System.out.println("User currently on \"" + titleOfPage.getText() + "\"");
            }

        }
        getDriver().switchTo().window(oldTab);
    }

    public void clickLogOffLink() {
        logOffLink.waitUntilVisible().click();
        System.out.println(notificationMessage.getText());
    }
}