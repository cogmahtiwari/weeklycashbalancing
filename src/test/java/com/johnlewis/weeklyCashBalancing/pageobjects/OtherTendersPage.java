package com.johnlewis.weeklyCashBalancing.pageobjects;

import com.johnlewis.weeklyCashBalancing.comman.WebdriverHelp;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class OtherTendersPage extends PageObject {

    @FindBy(id = "systemLogo")
    WebElementFacade homeButton;

    @FindBy(id = "headerSystemTitle")
    WebElementFacade titleOfPage;

    @FindBy(id = "otherTenders")
    WebElementFacade otherTenders;

    @FindBy(id = "tradingDateOutsideForm")
    WebElementFacade tradingDate;

    @FindBy(xpath = "//td[@class='tdScrollable']//span[contains(text(),'S01')]/parent::td/following-sibling::td/input[@type='text']")
    List<WebElementFacade> inputTenderTextBoxList;

    @FindBy(xpath = "//input[@class='tdNumber inputReadOnly  ']/parent::td/preceding-sibling::td//span")
    List<WebElementFacade> redonlySectionList;

    @FindBy(xpath = "//td[@class='tdScrollable']//span[contains(text(),'S')]/parent::td/following-sibling::td/input[@type='text' and @class='tdNumber   ']")
    List<WebElementFacade> inputTenderTextBoxReadOnlyList;

    @FindBy(xpath = "//span[contains(@class,'noBreak message')]")
    WebElementFacade notificationMessage;

    @FindBy(id = "//button[contains(text(),'Return to Weekly Cash Balancing')]")
    WebElementFacade systemErrorButton;

    @FindBy(xpath = "//h1[contains(text(),'System Error')]")
    WebElementFacade systemErrorPage;

    WebdriverHelp driverHelp = new WebdriverHelp();

    public void clickOtherTenders() {
        if (titleOfPage.getText().equalsIgnoreCase("Welcome to Weekly Cash Balancing"))
            otherTenders.waitUntilVisible().click();
        else if (systemErrorPage.getText().equalsIgnoreCase("System Error")) {
            systemErrorButton.waitUntilVisible().click();
            otherTenders.waitUntilVisible().click();
        } else {
            if (driverHelp.isAlertPresent(getDriver()) == true) {
                driverHelp.AlertMessage(getDriver(), "accept");
            } else {
                homeButton.waitUntilVisible().click();
                otherTenders.waitUntilVisible().click();
            }
        }
    }

    public void enterValueInTenders() {
        LinkedHashMap<String, String> valuetToInputList = new LinkedHashMap<>();
        valuetToInputList.put("Number", "100");
        valuetToInputList.put("Alphabets", "abcd");
        valuetToInputList.put("SpecialChar", "!£$%&*");
        valuetToInputList.put("LargeNumber", "99999999999999");
        valuetToInputList.put("NegativeValue", "-200");
        for (int i = 0; i < 5; i++) {
            for (Map.Entry<String, String> value : valuetToInputList.entrySet()) {
                inputTenderTextBoxList.get(i).clear();
                System.out.println("Key float as : " + value.getKey() + "-> Value as " + value.getValue());
                inputTenderTextBoxList.get(i).sendKeys(value.getValue());
                System.out.println("key float value : " + inputTenderTextBoxList.get(i).getAttribute("value"));
            }
        }
    }

    public void sectionS00AndS99Protected() {
        List<String> sectionNameList = new ArrayList<>();
        for (WebElement element : redonlySectionList)
            sectionNameList.add(element.getText());

        if (sectionNameList.contains("S00 UNKNOWN") && sectionNameList.contains("S99 FLOAT CONTROL"))
            System.out.println("Section S00 and S99 are protected");
        else
            System.out.println("Section S00 and S99 are NOT protected");
    }

    public void enterTradeDate(String date) {
        tradingDate.waitUntilVisible().sendKeys(date);
        System.out.println("Trading date Enter : " + date);
        if (driverHelp.isAlertPresent(getDriver()) == true) {
            driverHelp.AlertMessage(getDriver(), "accept");
        }
    }

    public void checkFieldStatus() {

        if (!(inputTenderTextBoxReadOnlyList.size() > 0))
            System.out.println(notificationMessage.getText());
        else
            System.out.println("Please enter other tender details");
    }
}
