package com.johnlewis.weeklyCashBalancing.pageobjects;

import com.johnlewis.weeklyCashBalancing.comman.WebdriverHelp;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ViewandSubmitWeeklyTotalsPage extends PageObject {

    @FindBy(id = "systemLogo")
    WebElementFacade homeButton;

    @FindBy(id = "headerSystemTitle")
    WebElementFacade titleOfPage;

    @FindBy(id = "viewAndUpload")
    WebElementFacade viewAndSubmitWeeklyTotalsButton;

    @FindBy(id = "tradingWeek")
    WebElementFacade tradingWeek;

    @FindBy(xpath = "//thead[@class='scrollable-thead']//tr//th")
    List<WebElementFacade> headerTitelList;

    @FindBy(xpath = "//thead[@class='scrollable-thead']//tr//td")
    List<WebElementFacade> headerValueList;

    @FindBy(xpath = "//a[contains(text(),'Submit')]")
    WebElementFacade submitButton;

    @FindBy(xpath = "//button[contains(text(),'Return to Weekly Cash Balancing')]")
    WebElementFacade systemErrorButton;

    WebdriverHelp driverHelp = new WebdriverHelp();

    public void clickviewAndSubmitWeeklyTotals() {
        if (titleOfPage.getText().equalsIgnoreCase("Welcome to Weekly Cash Balancing"))
            viewAndSubmitWeeklyTotalsButton.waitUntilVisible().click();
        else if (getDriver().getTitle().equalsIgnoreCase("Weekly Cash Balancing - error page")) {
            systemErrorButton.waitUntilVisible().click();
            viewAndSubmitWeeklyTotalsButton.waitUntilVisible().click();
        } else {
            driverHelp.jsClick(getDriver(), homeButton);
            if (driverHelp.isAlertPresent(getDriver()) == true) {
                driverHelp.AlertMessage(getDriver(), "accept");
            }
            viewAndSubmitWeeklyTotalsButton.waitUntilVisible().click();
        }
    }

    public void enterTradingWeek(List<String> week) {

        List<String> tradingWeekList = new ArrayList<>();
        tradingWeekList.addAll(tradingWeek.getSelectOptions());
        for (String weekValue : week) {
            for (String value : tradingWeekList) {
                if (value.trim().endsWith(driverHelp.getRequiredWeek(weekValue))) {
                    tradingWeek.selectByVisibleText(value);
                    System.out.println(value.trim() + " as \"" + weekValue + "\" trading week is selected");
                    List<String> headerTitleTextList = new LinkedList<>();
                    List<String> headerValueTextList = new LinkedList<>();

                    for (WebElement element : headerTitelList) {
                        headerTitleTextList.add(element.getText().replaceAll("\n", " "));
                    }
                    for (WebElement element : headerValueList) {
                        headerValueTextList.add(element.getText().replaceAll("\n", " "));
                    }
                    for (String str : headerTitleTextList) {
                        System.out.print("|" + str);
                    }
                    System.out.println();
                    for (String str : headerValueTextList) {
                        if (str.equalsIgnoreCase(""))
                            System.out.print(("|" + str + "         "));
                        else
                            System.out.print(("|" + str));
                    }
                    System.out.println();
                    break;
                }
            }
        }
    }

    public void submitWeeklyTotals() {
        List<String> tradingWeekList = new ArrayList<>();
        tradingWeekList.addAll(tradingWeek.getSelectOptions());
        int i = 0;
        for (String value : tradingWeekList) {
            tradingWeek.selectByVisibleText(value);
            while (submitButton.isCurrentlyVisible()) {
                System.out.println(value.trim() + " trading week is selected");
//                    submitButton.waitUntilVisible().click();
                Actions actions = new Actions(getDriver());
                actions.moveToElement(submitButton).click().perform();
//                if (driverHelp.isAlertPresent(getDriver()) == true) {
                driverHelp.AlertMessage(getDriver(), "reject");
//                    if (driverHelp.isAlertPresent(getDriver()) == true) {
                driverHelp.AlertMessage(getDriver(), "accept");
//                    }
                break;
//                }
            }
            while (submitButton.isCurrentlyVisible()) {
                Actions actions = new Actions(getDriver());
                actions.moveToElement(submitButton).click().perform();
//                if (driverHelp.isAlertPresent(getDriver()) == true) {
                driverHelp.AlertMessage(getDriver(), "accept");
                i++;
                break;
//                }
            }
            if (i == 1) {
                break;
            }
        }
    }
}