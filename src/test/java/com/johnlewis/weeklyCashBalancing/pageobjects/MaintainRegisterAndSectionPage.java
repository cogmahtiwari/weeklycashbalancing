package com.johnlewis.weeklyCashBalancing.pageobjects;

import com.johnlewis.weeklyCashBalancing.comman.WebdriverHelp;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.Iterator;
import java.util.List;

public class MaintainRegisterAndSectionPage extends PageObject {

    @FindBy(id = "systemLogo")
    WebElementFacade homeButton;

    @FindBy(id = "headerSystemTitle")
    WebElementFacade titleOfPage;

    @FindBy(xpath = "//button[contains(text(),'Return to Weekly Cash Balancing')]")
    WebElementFacade systemErrorButton;

    @FindBy(id = "sectionAndRegister")
    WebElementFacade maintainsectionAndRegister;

    @FindBy(xpath = "//th[@class='thScrollable']")
    WebElementFacade section;

    @FindBy(xpath = "//table[@class='scrollable-table']//td[@class='tdScrollable']//span[contains(text(),'S')]")
    List<WebElementFacade> sectionList;

    @FindBy(xpath = "//table[@class='scrollable-table']//td[@class='tdScrollable']//a//span[contains(text(),'S')]")
    List<WebElementFacade> sectionWithRegList;

    @FindBy(xpath = "//table[@class='scrollable-table']//td[@class='tdScrollable']/span[contains(text(),'S')]")
    List<WebElementFacade> onlySectionList;

    @FindBy(xpath = "//tr[@class='trExpanded']//div[@class='register']")
    List<WebElementFacade> expandedRegisterNameList;

    @FindBy(xpath = " //span[contains(text(),'Registers not assigned to any section')]")
    WebElementFacade unassignedRegister;

    @FindBy(id = "iconEdit")
    WebElementFacade editDetailsButton;

    @FindBy(xpath = "//td[@class='tdScrollable']//input[@name='sectionOrRegisterName' and @class='inputReadOnly  ']")
    List<WebElementFacade> editReadOnlySectionList;

    @FindBy(xpath = "//td[@class='tdScrollable']//input[@name='sectionOrRegisterName' and @class='inputReadOnly  ']/parent::td/preceding-sibling::td")
    List<WebElementFacade> editReadOnlySectionNumberList;

    @FindBy(xpath = "//td[@class='tdScrollable']//a[contains(text(),'S9')]")
    List<WebElementFacade> disabledSectionLIst;

    @FindBy(xpath = "//input[@disabled='']")
    List<WebElementFacade> disabledRegisterList;

    @FindBy(xpath = "//input[@disabled='']/parent::td/preceding-sibling::td//div")
    List<WebElementFacade> disabledRegisterNumberList;

    @FindBy(xpath = "//td[@class='tdScrollable']//input[@name='sectionOrRegisterName' and @class='  ' or @class='inputChanged']")
    List<WebElementFacade> editSectionList;

    @FindBy(id = "iconSave")
    WebElementFacade saveButton;

    @FindBy(xpath = "//span[contains(@class,'noBreak message')]")
    WebElementFacade notificationMessage;

    @FindBy(xpath = "//td[@class='tdScrollable']//a[contains(text(),'S01')]")
    WebElementFacade s01SectionLink;

    @FindBy(xpath = "//tr[@class='trExpanded']//input[@name='sectionOrRegisterName'or @class='inputChanged'] ")
    List<WebElementFacade> editRegisterList;

    @FindBy(xpath = "//tr[@id='tr_section_S01_register_0']//div")
    WebElementFacade s01RegisterNumber;

    @FindBy(xpath = "//tr[@id='tr_section_S01_register_0']//select")
    WebElementFacade sectionName;

    @FindBy(id = "iconAddReg")
    WebElementFacade addRegisterButton;

    @FindBy(id = "registerNumber_0")
    WebElementFacade registerNumber;

    @FindBy(id = "registerDescription_0")
    WebElementFacade regDescription;

    @FindBy(id = "registerSectionCode_0")
    WebElementFacade sectionNameAndDescription;

    @FindBy(id = "iconCancel")
    WebElementFacade cancelButton;

    @FindBy(id = "iconAddSection")
    WebElementFacade addSectionButton;

    @FindBy(id = "sectionCode_0")
    WebElementFacade sectionNumber;

    @FindBy(id = "sectionName_0")
    WebElementFacade sectionDescription;

    @FindBy(id = "iconListRegisters")
    WebElementFacade listAllRegButton;

    @FindBy(xpath = "//table[@class='scrollable-table']//tbody//tr")
    List<WebElementFacade> listedRegisterCount;

    WebdriverHelp driverHelp = new WebdriverHelp();

    public void clickMaintainRegisterAndSection() {
        if (getDriver().getTitle().equalsIgnoreCase("Weekly Cash Balancing - error page")) {
            systemErrorButton.waitUntilVisible().click();
            maintainsectionAndRegister.waitUntilVisible().click();
        } else if (titleOfPage.getText().equalsIgnoreCase("Welcome to Weekly Cash Balancing"))
            maintainsectionAndRegister.waitUntilVisible().click();
        else {
            driverHelp.jsClick(getDriver(), homeButton);
            if (driverHelp.isAlertPresent(getDriver()) == true) {
                driverHelp.AlertMessage(getDriver(), "accept");
            }
//            clickHomeButton();
            maintainsectionAndRegister.waitUntilVisible().click();

        }
    }

    public void sortedSectionsList() {
        System.out.println("Section sorted by \"" + section.getText() + "\"");
        System.out.println("===================");
        for (WebElement sectionElement : sectionList) {
            System.out.println(sectionElement.getText());
        }
    }

    public void clickSections() {
        for (WebElement sectionElement : sectionList) {
            sectionElement.click();
            System.out.println(sectionElement.getAttribute("innerHTML"));
            if (expandedRegisterNameList.size() > 0) {
                for (WebElement registerElement : expandedRegisterNameList) {
                    System.out.println("    " + registerElement.getText());
                }
                sectionElement.click();
            }
        }
        /*for(WebElement sectionElement:onlySectionList){
            System.out.println(sectionElement.getAttribute("innerHTML"));
        }*/
        System.out.println(unassignedRegister.getText());
        unassignedRegister.click();
        for (WebElement registerElement : expandedRegisterNameList) {
            System.out.println("    " + registerElement.getText());
        }
        unassignedRegister.click();
    }

    public void editSectionsAndRegister() {
        editSectionName();
        duplicateSectionName();
        editRegister();
        moveRegister();
        editDetailsButton.waitUntilVisible().click();
        System.out.println("Non editable section list");
        System.out.println("============================");
        Iterator<WebElementFacade> itr1 = editReadOnlySectionList.iterator();
        Iterator<WebElementFacade> itr2 = editReadOnlySectionNumberList.iterator();
        while (itr1.hasNext() && itr2.hasNext()) {
            System.out.println(itr2.next().getText() + " " + itr1.next().getAttribute("value"));
        }
        /*if (disabledRegisterNumberList.size() > 0 && disabledRegisterList.size() > 0) {
            System.out.println("Non editable register list");
            System.out.println("============================");
            for (WebElement section : disabledSectionLIst) {
                section.click();
            }
            Iterator<WebElementFacade> itr3 = disabledRegisterNumberList.iterator();
            Iterator<WebElementFacade> itr4 = disabledRegisterList.iterator();
            while (itr3.hasNext() && itr4.hasNext()) {
                System.out.println(itr3.next().getText() + " " + itr4.next().getAttribute("value"));
            }
        }*/

        /*List<String> sectionNameList = new ArrayList<>();
        for (WebElement sectionElement : ) {
            System.out.println(sectionElement.getAttribute("value"));
            sectionNameList.add(sectionElement.getAttribute("value"));
        }
        if (sectionNameList.contains("UNKNOWN") && sectionNameList.contains("FLOAT CONTROL"))
            System.out.println("Section name for S00 and S99 are NOT editable");
        else
            System.out.println("Section name for S00 and S99 are editable");*/

    }

    public void editSectionName() {
        editDetailsButton.waitUntilVisible().click();
        String currentSectionName = editSectionList.get(0).getAttribute("value");
        System.out.println("==================");
        System.out.println("Edit section name");
        System.out.println("==================");
        int i = 0;
        while (i < 2) {
            editSectionList.get(0).clear();
            if (i == 0) {
                editSectionList.get(0).sendKeys("Testing section name");
                System.out.println("Section Name " + currentSectionName + " changed to : " + editSectionList.get(0).getAttribute("value"));
            } else {
                editSectionList.get(0).sendKeys(currentSectionName);
                System.out.println("Section Name  Testing section name changed to : " + editSectionList.get(0).getAttribute("value"));
            }
            saveButton.waitUntilVisible().click();
            System.out.println(notificationMessage.getText());
            if (i == 0) {
                System.out.println("Reverting the section name to original");
                editDetailsButton.waitUntilVisible().click();
            }
            i++;
        }
    }

    public void duplicateSectionName() {
        editDetailsButton.waitUntilVisible().click();
        String sectionName = editSectionList.get(1).getAttribute("value");
        System.out.println("========================");
        System.out.println("Duplicate section name ");
        System.out.println("========================");
        clickMaintainRegisterAndSection();
        editDetailsButton.waitUntilVisible().click();
        editSectionList.get(0).clear();
        editSectionList.get(0).sendKeys(sectionName);
        saveButton.waitUntilVisible().click();
        System.out.println("Error details : " + notificationMessage.waitUntilVisible().getText());
    }

    public void clickHomeButton() {
        driverHelp.jsClick(getDriver(), homeButton);
    }

    public void editRegister() {
        clickHomeButton();
        driverHelp.AlertMessage(getDriver(), "accept");
        System.out.println("============================");
        System.out.println("Edit register ");
        System.out.println("============================");
        clickMaintainRegisterAndSection();
        int i = 0;
        String currentRegName = "";
        while (i < 2) {
            editDetailsButton.waitUntilVisible().click();
            s01SectionLink.waitUntilVisible().click();
            if (i == 0)
                currentRegName = editRegisterList.get(0).getAttribute("value");
            editRegisterList.get(0).clear();
            if (i == 0) {
                editRegisterList.get(0).sendKeys("New Testing Reg");
                System.out.println("New register name for " + currentRegName + " : " + editRegisterList.get(0).getAttribute("value"));
            } else {
                editRegisterList.get(0).sendKeys(currentRegName);
                System.out.println("New register name for New Testing Reg : " + editRegisterList.get(0).getAttribute("value"));
            }
            saveButton.waitUntilVisible().click();
            System.out.println(notificationMessage.getText());
            i++;
        }
    }

    public void moveRegister() {
        System.out.println("============================");
        System.out.println("Moving register to another section");
        System.out.println("============================");

        //moving the register
        editDetailsButton.waitUntilVisible().click();
        s01SectionLink.waitUntilVisible().click();
        String regNumber = s01RegisterNumber.getText();
        String currentSectionName = sectionName.getSelectedVisibleTextValue().substring(15, 44).substring(0, 3);

        String value = "Register " + s01RegisterNumber.getText() + " from section " + sectionName.getSelectedVisibleTextValue().substring(15, 44);
        sectionName.waitUntilVisible().selectByIndex(4);
        System.out.println(value + "is moved to section " + sectionName.getSelectedVisibleTextValue().substring(15, 44));
        String sectionNumber = sectionName.getSelectedVisibleTextValue().substring(15, 44).substring(0, 3);
        saveButton.waitUntilVisible().click();
        System.out.println(notificationMessage.getText());
        //reverting changes
        editDetailsButton.waitUntilVisible().click();
        WebElement sectionNumberElement = getDriver().findElement(By.xpath("//td[@class='tdScrollable']//a[contains(text(),'" + sectionNumber + "')]"));
        sectionNumberElement.click();

        Select selectSection = new Select(getDriver().findElement(By.xpath("//div[@class='register' and contains(text(),'" + regNumber + "')]/parent::td/following-sibling::td/select")));
        value = "Register " + regNumber + " from section " + selectSection.getFirstSelectedOption().getText().substring(15, 44);
        selectSection.selectByValue(currentSectionName);
        System.out.println(value + "is moved to section " + selectSection.getFirstSelectedOption().getText().substring(15, 44));
        saveButton.waitUntilVisible().click();
        System.out.println(notificationMessage.getText());
    }

    public void addNewRegister(String regNumber, String description, String sectionNumber) {
        //add new register
        clickMaintainRegisterAndSection();
        addRegisterButton.waitUntilVisible().click();
        registerNumber.sendKeys(regNumber);
        regDescription.sendKeys(description);
        if (!(sectionNumber.equalsIgnoreCase("")))
            sectionNameAndDescription.selectByValue(sectionNumber);
    }

    public void saveDetails() {
        saveButton.waitUntilVisible().click();
        if (notificationMessage.getText().contains("added successfully")) {
//            System.out.println("Register \"" + regNumber + "\" " + description + " to section \"" + sectionNumber + "\" " + notificationMessage.getText());
            System.out.println("Notification Details : "+notificationMessage.getText());
        } else {
//            System.out.println("Register \"" + regNumber + "\" " + description + " to section \"" + sectionNumber + "\" " + notificationMessage.getText());
            System.out.println("Notification Details : "+notificationMessage.getText());
            cancelButton.waitUntilVisible().click();
        }
    }

    public void addNewSection(String section, String description) {
        //add new section
        clickMaintainRegisterAndSection();
        addSectionButton.waitUntilVisible().click();
        JavascriptExecutor jse = (JavascriptExecutor) getDriver();
        jse.executeScript("arguments[0].value='" + section + "';", sectionNumber);
//        sectionNumber.sendKeys(Keys.CONTROL,'v'+section);
        sectionDescription.sendKeys(description);
        /*saveButton.waitUntilVisible().click();
        if (notificationMessage.getText().equalsIgnoreCase("Section(s) added successfully"))
            System.out.println("Section \"" + section + "\" " + description + " is : " + notificationMessage.getText());
        else {
            System.out.println("Section \"" + section + "\" " + description + " : " + notificationMessage.getText());
            cancelButton.waitUntilVisible().click();
        }*/
    }

    public void clickListAllRegisters() {
        listAllRegButton.waitUntilVisible().click();
        if (titleOfPage.getText().equalsIgnoreCase("List Registers"))
            System.out.println("List All Registers button clicked " + listedRegisterCount.size() + " is the count of listed register");
        else
            System.out.println("List All Registers button is disabled");
    }

}
