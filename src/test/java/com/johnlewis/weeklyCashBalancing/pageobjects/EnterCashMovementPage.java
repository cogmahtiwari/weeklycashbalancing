package com.johnlewis.weeklyCashBalancing.pageobjects;

import com.johnlewis.weeklyCashBalancing.comman.WebdriverHelp;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.Alert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class EnterCashMovementPage extends PageObject {

    @FindBy(id = "systemLogo")
    WebElementFacade homeButton;

    @FindBy(id = "tradingDateOutsideForm")
    WebElementFacade tradingDate;

    @FindBy(xpath = "//span[contains(@class,'noBreak message')]")
    WebElementFacade notificationMessage;

    @FindBy(xpath = "//a[contains(text(),'Save')]")
    WebElementFacade saveButton;

    @FindBy(id = "iconCancel")
    WebElementFacade cancelButton;

    @FindBy(id = "headerSystemTitle")
    WebElementFacade titleOfPage;

    @FindBy(xpath = "//button[contains(text(),'Return to Weekly Cash Balancing')]")
    WebElementFacade systemErrorButton;

    @FindBy(id = "cashMovement")
    WebElementFacade enterCashMovement;

    @FindBy(id = "movementType")
    WebElementFacade movementType;

    @FindBy(id = "fromSection")
    WebElementFacade fromSection;

    @FindBy(id = "fromRegister")
    WebElementFacade fromRegister;

    @FindBy(id = "toSection")
    WebElementFacade toSection;

    @FindBy(id = "toRegister")
    WebElementFacade toRegister;

    @FindBy(id = "movementValue")
    WebElementFacade movementValue;

    WebdriverHelp driverHelp = new WebdriverHelp();
    HomePage homePage = new HomePage();

    public void clickEnterCashMovement() {
        if (titleOfPage.getText().equalsIgnoreCase("Welcome to Weekly Cash Balancing"))
            enterCashMovement.waitUntilVisible().click();
        else if (getDriver().getTitle().equalsIgnoreCase("Weekly Cash Balancing - error page")) {
            systemErrorButton.waitUntilVisible().click();
            enterCashMovement.waitUntilVisible().click();
        } else {
            driverHelp.jsClick(getDriver(), homeButton);
            if (driverHelp.isAlertPresent(getDriver()) == true) {
                driverHelp.AlertMessage(getDriver(), "accept");
            }
            enterCashMovement.waitUntilVisible().click();
        }
    }

    public void enterTradingDate(List<String> day) {
        for (String dayValue : day) {
            if (dayValue.toLowerCase().equalsIgnoreCase("saturday")) {
                String date = "";
                do {
                    if (dayValue.equalsIgnoreCase("saturday"))
                        date = driverHelp.getRequiredDate(dayValue);
                    else
                        date = driverHelp.getRequiredSat(dayValue, date);
                    ((JavascriptExecutor) getDriver()).executeScript("document.getElementById('tradingDateOutsideForm').value=''");
                    tradingDate.getText();
                    tradingDate.waitUntilVisible().sendKeys(date);
                    if (driverHelp.isAlertPresent(getDriver()) == true) {
//                driverHelp.AlertMessage(getDriver(), "accept");
                        Alert messageAlert = getDriver().switchTo().alert();
                        messageAlert.accept();
                    }
                    System.out.println("Trading date Enter : " + date);
                    if (driverHelp.isAlertPresent(getDriver()) == true) {
                        driverHelp.AlertMessage(getDriver(), "accept");
                    }
                    dayValue = "again saturday";
                    if (!notificationMessage.isCurrentlyVisible())
                        break;
                } while (notificationMessage.getText().contains("this page is read only"));

            } else {
                String date = driverHelp.getRequiredDate(dayValue);
                ((JavascriptExecutor) getDriver()).executeScript("document.getElementById('tradingDateOutsideForm').setAttribute('value','')");
                tradingDate.waitUntilVisible().sendKeys(date);
                if (driverHelp.isAlertPresent(getDriver()) == true) {
//                driverHelp.AlertMessage(getDriver(), "accept");
                    Alert messageAlert = getDriver().switchTo().alert();
                    messageAlert.accept();
                }
                System.out.println("Trading date Enter : " + driverHelp.getRequiredDate(dayValue));
                if (driverHelp.isAlertPresent(getDriver()) == true) {
                    driverHelp.AlertMessage(getDriver(), "accept");
                }
            }
        }

    }

    public void dayValidation(List<String> days) {
        for (String value : days) {
            if (value.equalsIgnoreCase("future")) {
                System.out.println("Error detail : " + notificationMessage.getText());
            } else {
                System.out.println("Please enter the cash movement details for " + value + " date");
            }
        }
    }

    public void movementStatus() {
        System.out.println("Error detail : " + notificationMessage.getText());
    }

    public void checkForEditableFields() {
        if (movementType.isCurrentlyVisible() == false && fromSection.isCurrentlyVisible() == false && fromRegister.isCurrentlyVisible() == false && toSection.isCurrentlyVisible() == false && toRegister.isCurrentlyVisible() == false && movementValue.isCurrentlyVisible() == false) {
            System.out.println("Weekly total is submitted for selected day of week");
        } else
            System.out.println("Cash Movement screen not in sych with daily WCB total screen");
    }

    public void saveMovementDetails() {
        driverHelp.jsClick(getDriver(), saveButton);
        if (driverHelp.isAlertPresent(getDriver()) == true) {
            driverHelp.AlertMessage(getDriver(), "accept");
        }
//        System.out.println("Error detail :" + notificationMessage.getText());
    }

    public void selectMovementType() {
        movementType.sendKeys(Keys.RETURN);
        movementType.sendKeys(Keys.ARROW_DOWN);
        movementType.sendKeys(Keys.RETURN);
    }

    public void selectFromSectionAndRegister() {
        fromSection.sendKeys(Keys.RETURN);
        fromSection.sendKeys(Keys.ARROW_DOWN);
        fromSection.sendKeys(Keys.RETURN);
        fromRegister.sendKeys(Keys.RETURN);
        fromRegister.sendKeys(Keys.ARROW_DOWN);
        fromRegister.sendKeys(Keys.RETURN);
    }

    public void selectTOSectionAndRegister() {
        toSection.sendKeys(Keys.RETURN);
        toSection.sendKeys(Keys.ARROW_DOWN);
        toSection.sendKeys(Keys.RETURN);
        toRegister.sendKeys(Keys.RETURN);
        toRegister.sendKeys(Keys.ARROW_DOWN);
        toRegister.sendKeys(Keys.RETURN);
    }

    public void validateMovement() {

        System.out.println("Select only movement type then try to save the details");
        enterTradingDate(Arrays.asList("Today's"));
        //movement Type & save
        selectMovementType();
        saveMovementDetails();
        //movement type + from & save
        System.out.println("Select movement type,from section & register then try to save the details");
        selectFromSectionAndRegister();
        saveMovementDetails();
        //movementtype+from+value+save
        System.out.println("Select movement type,from section & register and movement value then try to save the details");
        movementValue.waitUntilVisible().sendKeys("100");
        saveMovementDetails();
        //Movement type+ from and to +save
        System.out.println("Select movement type,to & from section & register with out movement value then try to save the details");
        selectTOSectionAndRegister();
        saveMovementDetails();
        //Movement type+ from and to +again movement type
        System.out.println("Select all movement details then change the movement type");
        checkFromAndToSectionAndRegisterValue();
        //movementtype+to+save
        System.out.println("Select movement type,to section & register then try to save the details");
        selectMovementType();
        selectTOSectionAndRegister();
        saveMovementDetails();
        //movementtype+to+value+save
        System.out.println("Select movement type,to section & register and movement value then try to save the details");
        movementValue.waitUntilVisible().sendKeys("100");
        saveMovementDetails();

    }

    public void checkFromAndToSectionAndRegisterValue() {
        movementValue.waitUntilVisible().sendKeys("100");
        movementType.sendKeys(Keys.RETURN);
        movementType.sendKeys(Keys.ARROW_DOWN);
        movementType.sendKeys(Keys.ARROW_DOWN);
        movementType.sendKeys(Keys.RETURN);
        if (fromSection.getText().contains("--Select--\n") && (toSection.getText().contains("--Select--\n"))/*&&movementValue.getValue().equals("")*/) {
            System.out.println("From and to section and register details are refreshed ");
        } else
            System.out.println("From and to section and register details are not refreshed ");
    }

    public void ValidateMovementValue() {
        System.out.println("\nVerify movement value");
        System.out.println("========================");
        enterTradingDate(Arrays.asList("Today's"));
        selectMovementType();
        selectFromSectionAndRegister();
        selectTOSectionAndRegister();

        LinkedHashMap<String, String> valueToInputList = new LinkedHashMap<>();
        valueToInputList.put("Number", "100");
        valueToInputList.put("Alphabets", "abcd");
        valueToInputList.put("SpecialChar", "!£$%&*");
        valueToInputList.put("LargeNumber", "99999999999999");
        valueToInputList.put("NegativeValue", "-200");
        for (Map.Entry<String, String> value : valueToInputList.entrySet()) {
            movementValue.clear();
            movementValue.sendKeys(value.getValue());
            System.out.println("Key float as : " + value.getKey() + "-> Value as " + value.getValue());
            System.out.println("keyed float value : " + movementValue.getAttribute("value"));
        }
        saveButton.waitUntilVisible().click();
        if(notificationMessage.isCurrentlyVisible()){
            System.out.println("Notification Details: "+notificationMessage.getText());
        }
    }

    public void performActivity(List<String> activity) {
        int i = 1;

        for (String act : activity) {
            String date = driverHelp.getRequiredDate("today's");
            ((JavascriptExecutor) getDriver()).executeScript("document.getElementById('tradingDateOutsideForm').setAttribute('value','')");
            tradingDate.waitUntilVisible().sendKeys(date);
            if (driverHelp.isAlertPresent(getDriver()) == true) {
//                driverHelp.AlertMessage(getDriver(), "accept");
                Alert messageAlert = getDriver().switchTo().alert();
                messageAlert.accept();
            }
            if (!(act.equalsIgnoreCase("save blank"))) {
                movementType.sendKeys(Keys.RETURN);
                for (int j = 0; j < i; j++)
                    movementType.sendKeys(Keys.ARROW_DOWN);
                movementType.sendKeys(Keys.RETURN);

                fromSection.sendKeys(Keys.RETURN);
                for (int j = 0; j < i; j++)
                    fromSection.sendKeys(Keys.ARROW_DOWN);
                fromSection.sendKeys(Keys.RETURN);

                fromRegister.sendKeys(Keys.RETURN);
                for (int j = 0; j < i; j++)
                    fromRegister.sendKeys(Keys.ARROW_DOWN);
                fromRegister.sendKeys(Keys.RETURN);

                toSection.sendKeys(Keys.RETURN);
                for (int j = 0; j < i; j++)
                    toSection.sendKeys(Keys.ARROW_DOWN);
                toSection.sendKeys(Keys.RETURN);

                toRegister.sendKeys(Keys.RETURN);
                for (int j = 0; j < i; j++)
                    toRegister.sendKeys(Keys.ARROW_DOWN);
                toRegister.sendKeys(Keys.RETURN);

                movementValue.waitUntilVisible().sendKeys("100");
            }

            System.out.println(act + " Activity Started");
            System.out.println("============================");
            switch (act.toLowerCase()) {
                case "change date":
                    enterTradingDate(Arrays.asList("wednesday"));
                    if (driverHelp.isAlertPresent(getDriver()) == true) {
                        driverHelp.AlertMessage(getDriver(), "accept");
                    }
                    movementType.selectByIndex(0);//remove this line after movement type refresh code is deployed
                    break;
                case "change branch":
                    homePage.selectBranch("Peter Jones");
                    /*if (driverHelp.isAlertPresent(getDriver()) == true) {
                        driverHelp.AlertMessage(getDriver(), "accept");
                    }*/
                    clickEnterCashMovement();
//                    enterTradingDate(Arrays.asList("Today's"));
                    break;
                case "cancel movement":
                    cancelButton.waitUntilVisible().click();
//                    driverHelp.jsClick(getDriver(), cancelButton);
                    if (driverHelp.isAlertPresent(getDriver()) == true) {
                        driverHelp.AlertMessage(getDriver(), "accept");
                    }
                    clickEnterCashMovement();
                    break;
                case "click homebutton":
                    homeButton.waitUntilVisible().click();
                    clickEnterCashMovement();
                    break;
                case "save blank":
                    saveMovementDetails();
                    if (driverHelp.isAlertPresent(getDriver()) == true) {
                        driverHelp.AlertMessage(getDriver(), "accept");
                    }
                    break;
            }
            i++;
        }

    }
}
