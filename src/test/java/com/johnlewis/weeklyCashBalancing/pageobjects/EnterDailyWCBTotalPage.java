package com.johnlewis.weeklyCashBalancing.pageobjects;

import com.johnlewis.weeklyCashBalancing.comman.WebdriverHelp;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class EnterDailyWCBTotalPage extends PageObject {

    @FindBy(id = "wcbTotals")
    WebElementFacade wcbTotal;

    @FindBy(id = "tradingDateOutsideForm")
    WebElementFacade tradingDate;

    @FindBy(xpath = "//span[contains(@class,'noBreak message')]")
    WebElementFacade notificationMessage;

    @FindBy(xpath = "//tbody//tr//input[@name='floatClosing']")
    WebElementFacade floatClosingColumn;

    @FindBy(xpath = "//tbody//tr//input[@name='reservesClosing']")
    WebElementFacade reservesClosingColumn;

    @FindBy(id = "systemLogo")
    WebElementFacade homeButton;

    @FindBy(xpath = "//table//tbody//tr//input[@id='floatClosing_1' and @class='tdNumber   ']")
    List<WebElementFacade> closingFloatList;

    @FindBy(xpath = "//table//tbody//tr//input[@id='reservesClosing_1' and @class='tdNumber   ']")
    List<WebElementFacade> closingreservesList;

    @FindBy(xpath = "//th[@class='thScrollable']//a[contains(text(),\"Section\")]")
    WebElementFacade sectionList;

    @FindBy(xpath = "//td[@class='tdScrollable']//span[@class='noBreak' and contains(text(),\"S\")]")
    List<WebElementFacade> sectionNumberList;

    @FindBy(xpath = "//table[@id='scrollableTable']//tbody//tr//span[contains(text(),'S00')]")
    WebElementFacade sectionS00;

    @FindBy(xpath = "//span[contains(text(),'S00')]/ancestor::td/following-sibling::td//input[(@name='floatClosing' or @name='reservesClosing') and contains(@class,'inputReadOnly')]")
    List<WebElementFacade> s00CLROList;

    @FindBy(xpath = "//span[contains(text(),'S99')]/ancestor::td/following-sibling::td//input[(@name='floatClosing' or @name='reservesClosing') and contains(@class,'inputReadOnly')]")
    List<WebElementFacade> s99CLROList;

    @FindBy(xpath = "//span[contains(text(),'S99')]/ancestor::td/following-sibling::td//input[@name='adjustment' and contains(@class,'inputReadOnly')]")
    List<WebElementFacade> s99Adjustment;

    @FindBy(xpath = "//table[@id='scrollableTable']//tbody//tr//span[contains(text(),'S99')]")
    WebElementFacade sectionS99;

    @FindBy(xpath = "//tr[@class='trExpanded']//input[contains(@class,'ReadOnly')]")
    List<WebElementFacade> readOnlyExpandedRegisterFloatAndReservesList;

    @FindBy(xpath = "//tr[@class='trExpanded']//div[@class='register']")
    List<WebElementFacade> expandedRegisterNameList;

    @FindBy(xpath = "//tr[@class='trExpanded']//input[@name='floatClosingForRegister']")
    List<WebElementFacade> expandedClosingFloatList;

    @FindBy(xpath = "//tr[@class='trExpanded']//input[@name='floatClosingForRegister' and contains(@class,'ReadOnly')]")
    List<WebElementFacade> expandedClosingFloatReadOnlyList;

    @FindBy(xpath = "//tr[@class='trExpanded']//input[@name='reservesClosingForRegister']")
    List<WebElementFacade> expandedCloseReserversList;

    @FindBy(xpath = "//tr[@class='trExpanded']//input[@name='reservesClosingForRegister'and contains(@class,'ReadOnly')]")
    List<WebElementFacade> expandedCloseReserversReadOnlyList;

    @FindBy(id = "adjustment_0")
    WebElementFacade adjustmentAmount;

    @FindBy(id = "adjustmentId_0")
    WebElementFacade adjustmentReasonList;

    @FindBy(id = "adjustmentReason_0")
    WebElementFacade adjReasonText;

    @FindBy(xpath = "//a[contains(text(),'Save')]")
    WebElementFacade saveButton;

    @FindBy(id = "iconCancel")
    WebElementFacade cancelButton;

    @FindBy(id = "headerSystemTitle")
    WebElementFacade titleOfPage;

    @FindBy(xpath = "//button[contains(text(),'Return to Weekly Cash Balancing')]")
    WebElementFacade systemErrorButton;

    //@FindBy(xpath = "//span[contains(text(),'CHANGE') or contains(text(),'SAFE')]/anecestor::td/following-sibling::td//input[contains(@class,'inputReadOnly')]")
    @FindBy(xpath = "//span[contains(text(),'CHANGE') or contains(text(),'SAFE')]/parent::a/parent::td/following-sibling::td//input[contains(@class,'inputReadOnly')]")
    List<WebElementFacade> safeAndChangeMachineList;

    @FindBy(xpath = "//input[@class='tdNumber   ']")
    List<WebElementFacade> readOnlyTextBox;

    @FindBy(xpath = "//input[contains(@id,'adjustment') and @class='tdNumber   ']")
    List<WebElementFacade> movemnentValue;

    WebdriverHelp driverHelp = new WebdriverHelp();

    List<WebElementFacade> expandedSectionNumberList = new LinkedList<>();

    public void clickEnterDailyWCBTotal() {
        if (titleOfPage.getText().equalsIgnoreCase("Welcome to Weekly Cash Balancing"))
            wcbTotal.waitUntilVisible().click();
        else if (getDriver().getTitle().equalsIgnoreCase("Weekly Cash Balancing - error page")) {
            systemErrorButton.waitUntilVisible().click();
            wcbTotal.waitUntilVisible().click();
        } else {
            driverHelp.jsClick(getDriver(), homeButton);
            if (driverHelp.isAlertPresent(getDriver()) == true) {
                driverHelp.AlertMessage(getDriver(), "accept");
            }
            wcbTotal.waitUntilVisible().click();
        }
    }

    public void enterTradingDate(List<String> day) {
        for (String dayValue : day) {
            if (dayValue.toLowerCase().equalsIgnoreCase("saturday")) {
                String date = "";
                do {
                    if (dayValue.equalsIgnoreCase("saturday"))
                        date = driverHelp.getRequiredDate(dayValue);
                    else
                        date = driverHelp.getRequiredSat(dayValue, date);
                    ((JavascriptExecutor) getDriver()).executeScript("document.getElementById('tradingDateOutsideForm').value=''");
                    tradingDate.getText();
                    tradingDate.waitUntilVisible().sendKeys(date);
                    System.out.println("Trading date Enter : " + date);
                    if (driverHelp.isAlertPresent(getDriver()) == true) {
                        driverHelp.AlertMessage(getDriver(), "accept");
                    }
                    dayValue = "again saturday";
                    if (!notificationMessage.isCurrentlyVisible())
                        break;
                } while (notificationMessage.getText().contains("this page is read only"));

            } else {
                String date = driverHelp.getRequiredDate(dayValue);
                ((JavascriptExecutor) getDriver()).executeScript("document.getElementById('tradingDateOutsideForm').setAttribute('value','')");
                tradingDate.waitUntilVisible().sendKeys(date);
                System.out.println("Trading date Enter : " + driverHelp.getRequiredDate(dayValue));
                if (driverHelp.isAlertPresent(getDriver()) == true) {
                    driverHelp.AlertMessage(getDriver(), "accept");
                }
            }
        }

    }

    public void errorMessageDisplay() {
        System.out.println("Error detail : " + notificationMessage.getText());
    }

    public void clickHomeButton() {
        driverHelp.jsClick(getDriver(), homeButton);
        if (driverHelp.isAlertPresent(getDriver()) == true) {
            driverHelp.AlertMessage(getDriver(), "accept");
        }
    }

    public void acceptAlertMessage() {
        driverHelp.AlertMessage(getDriver(), "accept");
    }

    public void closeFloatAndReserveStatus() {
        if (closingFloatList.size() > 0 && closingreservesList.size() > 0) {
            System.out.println("Closing float & closing reserve are editable");
            for (WebElement element : closingFloatList) {
                element.clear();
                element.sendKeys("100");
            }
            for (WebElement element : closingreservesList) {
                element.clear();
                element.sendKeys("200");
            }
        } else
            System.out.println("Closing float & closing reserve are not editable other then saturday");

    }

    public void sortSection() {
        sectionList.waitUntilVisible().click();
    }

    public void selectSection() {
        for (int i = 0; i < sectionNumberList.size(); i++) {
            sectionNumberList.get(i).click();
            expandedSectionNumberList.addAll(expandedRegisterNameList);
            System.out.println("List of register for " + sectionNumberList.get(i).getText() + " section");
            for (WebElement register : expandedSectionNumberList) {
                System.out.println(register.getText());
            }
            sectionNumberList.get(i).click();
            break;
        }
    }

    public void enterFloatAtSectionLevel() {
        for (int i = 0; i < sectionNumberList.size(); i++) {
            sectionNumberList.get(i).click();
            WebElement currentSectionClosingFloat = sectionNumberList.get(i).findElement(By.xpath("./parent::a/parent::td/following-sibling::td//input[@name ='floatClosing' and @class='tdNumber   ']"));
            currentSectionClosingFloat.clear();
            currentSectionClosingFloat.sendKeys("10");
            WebElement currentSectionClosingReserves = sectionNumberList.get(i).findElement(By.xpath("./parent::a/parent::td/following-sibling::td//input[@name ='reservesClosing' and @class='tdNumber   ']"));
            currentSectionClosingReserves.clear();
            currentSectionClosingReserves.sendKeys("10" + Keys.TAB);
            if (expandedClosingFloatReadOnlyList.size() > 0 && expandedCloseReserversReadOnlyList.size() > 0)
                System.out.println("Register level float closing & reserves close are  protected");
            else
                System.out.println("Register level float closing & reserves close are NOT protected");
            currentSectionClosingFloat.clear();
            currentSectionClosingReserves.clear();
            sectionNumberList.get(i).click();
            break;
        }
    }

    public void enterFloatAtRegisterLevel() {
        /*if (expandedClosingFloatList.size() > 0 && expandedCloseReserversList.size() > 0) {
            for (WebElement element : expandedClosingFloatList) {
                element.sendKeys("1");
            }
            for (WebElement element : expandedCloseReserversList) {
                element.sendKeys("2");
            }
        }
        if (currentregisterClosingFloat.isCurrentlyVisible() && currentRegisterClosingReserves.isCurrentlyVisible())
            System.out.println("Section level float closing & reserves close is protected");
        else
            System.out.println("Section level float closing & reserves close is NOT protected");

        for (WebElement element : expandedClosingFloatList) {
            element.clear();
        }
        for (WebElement element : expandedCloseReserversList) {
            element.clear();
        }*/
    }

    public void enterValueInRegister() {
        for (int i = 0; i < sectionNumberList.size(); i++) {
            sectionNumberList.get(i).click();
            if (expandedRegisterNameList.size() > 5) {
                LinkedHashMap<String, String> valueToInputList = new LinkedHashMap<>();
                valueToInputList.put("Number", "100");
                valueToInputList.put("Alphabets", "abcd");
                valueToInputList.put("SpecialChar", "!£$%&*");
                valueToInputList.put("LargeNumber", "99999999999999");
                valueToInputList.put("NegativeValue", "-200");
                int index = 0;
                for (Map.Entry<String, String> value : valueToInputList.entrySet()) {
                    expandedClosingFloatList.get(index).clear();
                    System.out.println("Key float as : " + value.getKey() + "-> Value as " + value.getValue());
                    expandedClosingFloatList.get(index).sendKeys(value.getValue());
                    System.out.println("keyed float value : " + expandedClosingFloatList.get(index).getAttribute("value"));
                    expandedCloseReserversList.get(index).clear();
                    expandedCloseReserversList.get(index).sendKeys(value.getValue());
                    index++;
                }
                break;
            } else
                sectionNumberList.get(i).click();
        }
        saveButton.waitUntilVisible().click();
        if(notificationMessage.isCurrentlyVisible()){
            System.out.println("Notification details : "+notificationMessage.getText());
        }
    }

    public void verifyBusinessRule() {
        for (WebElement element : expandedRegisterNameList) {
            String sectionNumber = element.findElement(By.xpath("./ancestor::tr/td//input[contains(@name,'parent')]")).getAttribute("value");
            List<WebElement> currentRegisterClosingFloatList = getDriver().findElements(By.xpath("//span[contains(text(),'" + sectionNumber + "')]/ancestor::td/following-sibling::td//input[@name='floatClosing' and @class='tdNumber inputReadOnly']"));
            List<WebElement> currentRegisterClosingReservesList = getDriver().findElements(By.xpath("//span[contains(text(),'" + sectionNumber + "')]/ancestor::td/following-sibling::td//input[@name='reservesClosing' and @class='tdNumber inputReadOnly']"));
            if (currentRegisterClosingFloatList.size() > 0 && currentRegisterClosingReservesList.size() > 0) {
                System.out.println("Section level float closing & reserves close is protected");
                break;
            } else
                System.out.println("Section level float closing & reserves close is NOT protected");
        }

    }

    /*public void enterValueInAdjustment() {
        adjustmentAmount.clear();
        adjustmentAmount.sendKeys("100");
//        adjustmentReasonList.selectByVisibleText("Car Park Payment");
        adjReasonText.clear();
        //saving without giving reason details
        verifyAdjustmentReason();
        errorMessageDisplay();
        adjustmentAmount.click();
        adjustmentReasonList.selectByVisibleText("Other (please add freeform text)");
        //saving without put value in reason text box
        verifyAdjustmentReason();
        errorMessageDisplay();
        adjustmentAmount.click();
        System.out.println("Adjustment Amount : "+adjustmentAmount.getAttribute("value"));
        System.out.println("Adjustment reason : "+adjustmentReasonList.getSelectedVisibleTextValue().trim());
        adjReasonText.sendKeys("test . 9999 £$%& ");
        System.out.println("Reason Text :"+adjReasonText.getAttribute("value"));
        verifyAdjustmentReason();
    }
*/
  /*  public void verifyAdjustmentReason() {
//        driverHelp.jsClick(getDriver(), saveButton.waitUntilVisible());
        Actions actions = new Actions(getDriver());
        actions.moveToElement(saveButton).click().perform();
        if (driverHelp.isAlertPresent(getDriver()) == true) {
            driverHelp.AlertMessage(getDriver(), "accept");
        }
    }
*/
    public void cancelUserInput() {
//        cancelButton.waitUntilVisible().click();
        driverHelp.jsClick(getDriver(), cancelButton);
        if (driverHelp.isAlertPresent(getDriver()) == true) {
            driverHelp.AlertMessage(getDriver(), "accept");
        }
    }

    public void clickS00AndS99Sections() {
        sectionList.waitUntilVisible().click();
        sectionS00.click();
        System.out.println(sectionS00.getText() + " register's list");
        for (WebElement element : expandedRegisterNameList)
            System.out.println(element.getText());
        sectionS00.click();
        driverHelp.jsClick(getDriver(), sectionS99);
        if (expandedRegisterNameList.size() < 1) {
            System.out.println(sectionS99.getText() + " do not have the register");
        } else {
            System.out.println(sectionS99.getText() + " register's list");
            for (WebElement element : expandedRegisterNameList)
                System.out.println(element.getText());
        }
    }

    public void sooAndS99ClosingFloatAndReserveValidation() {
        if (s00CLROList.size() > 0 && s99CLROList.size() > 0)
            System.out.println("Closing float & closing reserve are protected for section S00 and S99");
        else
            System.out.println("Closing float & closing reserve are NOT protected for section S00 and S99");

        //daily wcb total safe float and change machine field disabled verification remove after main float screen development
        /*if (safeAndChangeMachineList.size() >= 24)
            System.out.println("Closing float,closing reserve and adjustment are protected for safe float and change machine");
        else
            System.out.println("Closing float,closing reserve and adjustment are NOT protected for safe float and change machine");*/
    }

    /*public void verifyS99Adjustment() {
        if (s99Adjustment.size() > 0)
            System.out.println("Adjustment is protected for section S99");
        else
            System.out.println("Adjustment is NOT protected for section S99");
    }
*/
    public void selectSubmittedDate() {
        String date = "";
        String dayValue = "saturday";
        do {
            if (dayValue.equalsIgnoreCase("saturday"))
                date = driverHelp.getRequiredDate(dayValue);
            else
                date = driverHelp.getRequiredSat(dayValue, date);
            ((JavascriptExecutor) getDriver()).executeScript("document.getElementById('tradingDateOutsideForm').setAttribute('value','');");
            tradingDate.waitUntilVisible().sendKeys(date);
            System.out.println("Trading date Enter : " + date);
            if (driverHelp.isAlertPresent(getDriver()) == true) {
                driverHelp.AlertMessage(getDriver(), "accept");
            }
            dayValue = "again saturday";
            if (notificationMessage.isCurrentlyVisible() && notificationMessage.getText().contains("already been submitted"))
                break;
        } while (!(driverHelp.getWebElement(notificationMessage) == null));

    }

    public void checkForEditableFields() {
        if (readOnlyTextBox.size() > 0)
            System.out.println("No editable fields for the this week");
        else
            System.out.println(readOnlyTextBox.size() + " number of editable fields for this week");
    }

    public void verifyMovementColumn() {
        if (movemnentValue.size() > 0)
            System.out.println("Movement value should not editable to user");
        else
            System.out.println("Movement value is visible to user");
    }
}