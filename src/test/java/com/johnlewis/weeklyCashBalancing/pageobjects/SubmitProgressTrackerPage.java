package com.johnlewis.weeklyCashBalancing.pageobjects;

import com.johnlewis.weeklyCashBalancing.comman.WebdriverHelp;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class SubmitProgressTrackerPage extends PageObject {

    private final static Logger log = LoggerFactory.getLogger(SubmitProgressTrackerPage.class);

    @FindBy(id = "systemLogo")
    WebElementFacade homeButton;

    @FindBy(id = "headerSystemTitle")
    WebElementFacade titleOfPage;

    @FindBy(xpath = "//img[contains(@id,'Progress')]")
    WebElementFacade submitProgressTracker;

    @FindBy(id = "tradingWeek")
    WebElementFacade tradingWeek;

    @FindBy(xpath = "//thead[@class='scrollable-thead']//tr//th")
    List<WebElementFacade> headerTitelList;

    @FindBy(xpath = "//tr[1]//td[contains(@class,'tdScrollable')]")
    List<WebElementFacade> headerValueList;

    @FindBy(xpath = "//tr[1]//td[contains(@class,'tdScrollable')]//img")
    WebElementFacade submittedStatusImg;

    @FindBy(xpath = "//table[@class='scrollable-table']//tbody//tr//td//img[@id='tick']")
    List<WebElementFacade> submittedBranchList;

    @FindBy(xpath = "//table[@class='scrollable-table']//tbody//tr//td[2]//img")
    List<WebElementFacade> statusdataTableList;

    @FindBy(xpath = "//table[@class='scrollable-table']//tbody//tr//td//img[@id='passback']")
    List<WebElementFacade> dataTableList;

    @FindBy(xpath = "//button[contains(text(),'Return to Weekly Cash Balancing')]")
    WebElementFacade systemErrorButton;

    WebdriverHelp driverHelp = new WebdriverHelp();

    public void clickSubmitProgressTracker() {
        if (titleOfPage.getText().equalsIgnoreCase("Welcome to Weekly Cash Balancing"))
            submitProgressTracker.waitUntilVisible().click();
        else if (getDriver().getTitle().equalsIgnoreCase("Weekly Cash Balancing - error page")) {
            systemErrorButton.waitUntilVisible().click();
            submitProgressTracker.waitUntilVisible().click();
        } else {
            driverHelp.jsClick(getDriver(), homeButton);
            if (driverHelp.isAlertPresent(getDriver()) == true) {
                driverHelp.AlertMessage(getDriver(), "accept");
            }
            submitProgressTracker.waitUntilVisible().click();
        }
    }

    public void viewProgressTrackerDetails(List<String> week) {
        List<String> tradingWeekList = new ArrayList<>();
        tradingWeekList.addAll(tradingWeek.getSelectOptions());
        for (String weekValue : week) {
            for (String value : tradingWeekList) {
                if (value.trim().endsWith(driverHelp.getRequiredWeek(weekValue))) {
                    tradingWeek.selectByVisibleText(value);
                    System.out.println(value.trim() + " as \"" + weekValue + "\" trading week is selected");
                    List<String> headerTitleTextList = new LinkedList<>();
                    List<String> headerValueTextList = new LinkedList<>();

                    for (WebElement element : headerTitelList) {
                        headerTitleTextList.add(element.getText().replaceAll("\n", " "));
                    }
                    for (int i = 0; i < headerValueList.size(); i++) {
                        if (i == 1)
                            headerValueTextList.add(submittedStatusImg.getAttribute("id").replaceAll("\n", " "));
                        else
                            headerValueTextList.add(headerValueList.get(i).getText().replaceAll("\n", " "));
                    }
                    for (String str : headerTitleTextList) {
                        if (str.equalsIgnoreCase(""))
                            System.out.print("|Submitted Status");
                        else
                            System.out.print("|" + str);
                    }
                    System.out.println();
                    for (String str : headerValueTextList) {
                        if (str.equalsIgnoreCase(""))
                            System.out.print(("|" + str + "         "));
                        else
                            System.out.print(("|" + str));
                    }
                    System.out.println();
                    break;
                }
            }
        }
    }

    public void checkPassBackStatus() {
        List<String> tradingWeekList = new ArrayList<>();
        tradingWeekList.addAll(tradingWeek.getSelectOptions());

        for (int i = 0; i < tradingWeekList.size(); i++) {
            tradingWeek.selectByIndex(i);
            System.out.println(tradingWeek.getSelectedVisibleTextValue().trim() + " trading week selected");
            if (submittedBranchList.size() > 0) {
                System.out.println("Pass back enabled branch list");
                System.out.println("=============================");

                for (WebElement element : statusdataTableList) {
                    if (element.getAttribute("id").equalsIgnoreCase("tick")) {
                        System.out.println(element.findElement(By.xpath("./parent::td/parent::tr")).getText());
                    }
                }
                break;
            } else
                System.out.println("No branch has submitted the daily totals for current week");
        }

    }

    public void submitPassBack() {
        List<String> tradingWeekList = new ArrayList<>();
        tradingWeekList.addAll(tradingWeek.getSelectOptions());

        for (int j = 0; j < tradingWeekList.size(); j++) {
            tradingWeek.selectByIndex(j);
            System.out.println(tradingWeek.getSelectedVisibleTextValue().trim() + " trading week selected");
            if (dataTableList.size() > 0) {
                for (WebElement element : dataTableList) {
                    for (int i = 1; i < 7; i++) {
                        if (i == 2)
                            System.out.print(element.findElement(By.xpath("./parent::td/parent::tr//td[" + i + "]//img")).getAttribute("id").trim() + " ");
                        else
                            System.out.print(element.findElement(By.xpath("./parent::td/parent::tr//td[" + i + "]")).getText().trim() + " ");
                    }
                    String branchName = element.findElement(By.xpath("./parent::td/parent::tr//td[1]")).getText().trim();
                    element.click();
                    System.out.println();
                    for (int i = 1; i < 7; i++) {
                        if (i == 2)
                            System.out.print("\"" + getDriver().findElement(By.xpath("//table[@class='scrollable-table']//tbody//tr//span[contains(text(),'" + branchName + "')]/parent::td/following-sibling::td//img")).getAttribute("id").trim() + "\" ");
                        else if (i == 1)
                            System.out.print(getDriver().findElement(By.xpath("//table[@class='scrollable-table']//tbody//tr//span[contains(text(),'" + branchName + "')]/parent::td[" + i + "]")).getText().trim() + " ");
                        else
                            System.out.print(getDriver().findElement(By.xpath("//table[@class='scrollable-table']//tbody//tr//span[contains(text(),'" + branchName + "')]/parent::td/following-sibling::td[" + (i - 1) + "]")).getText().trim() + " ");
                    }
                    System.out.println();
                    break;
                }
                break;
            } else
                System.out.println("No branch has submitted the daily totals for current week");
        }

    }
}