package com.johnlewis.weeklyCashBalancing.pageobjects;

import com.johnlewis.weeklyCashBalancing.comman.CreateBranchList;
import com.johnlewis.weeklyCashBalancing.comman.WebdriverHelp;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.WhenPageOpens;
import net.thucydides.core.pages.PageObject;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

public class HomePage extends PageObject {

    @FindBy(id = "j_username")
    WebElementFacade userId;

    @FindBy(id = "j_password")
    WebElementFacade password;

    @FindBy(id = "submitButton")
    WebElementFacade signInBtn;

    @FindBy(id = "branchNumber")
    WebElementFacade branchName;

    @FindBy(xpath = "//div[@id='divSwitchBranch']//a//span[@class='noBreak']")
    WebElementFacade branchLink;

    @FindBy(id = "submit")
    WebElementFacade selectButton;

    @FindBy(id = "headerSystemTitle")
    WebElementFacade titleOfPage;

    @FindBy(xpath = "//span[@class='noBreak messageError']")
    WebElementFacade notificationMessage;

    @FindBy(id = "main-frame-error")
    WebElementFacade pageDownError;

    @FindBy(xpath = "//span[@class='noBreak']")
    WebElementFacade currentVersion;

    @FindBy(xpath = "//span[@class='noBreak' and contains(text(),'Log')]")
    WebElementFacade logOffLink;

    @FindBy(xpath = "//button[contains(text(),'Return to Weekly Cash Balancing')]")
    WebElementFacade systemErrorButton;

    WebdriverHelp driverHelp = new WebdriverHelp();

    LinkedList<String>branchList;

    @WhenPageOpens
    public void BrowserMaximize() {
        getDriver().manage().window().maximize();
    }

    public void signInToMyAccount(String uId, String password) {
        if (logOffLink.isCurrentlyVisible())
            logOffLink.waitUntilVisible().click();
        System.out.println("Current version of application : " + currentVersion.getText());
        userId.waitUntilVisible().sendKeys(uId);
        this.password.waitUntilVisible().sendKeys(password);
        signInBtn.waitUntilVisible().click();
        System.out.println("Login details are entered");
    }

    public void selectBranch(String brName) {
        CreateBranchList branchList=new CreateBranchList();
        branchList.writeBranchList(brName);
        if (titleOfPage.getText().equalsIgnoreCase("Select Branch")) {
            branchName.selectByVisibleText(brName);
            selectButton.waitUntilVisible().click();
            System.out.println("Branch selected as : " + brName);
        } else if (titleOfPage.getText().equalsIgnoreCase("Log on to Weekly Cash Balancing") /*|| titleOfPage.getText().equalsIgnoreCase("Welcome to Weekly Cash Balancing")*/) {

        } else {
            driverHelp.jsClick(getDriver(), branchLink);
            if (driverHelp.isAlertPresent(getDriver()) == true) {
                driverHelp.AlertMessage(getDriver(), "accept");
            }
            branchName.waitUntilVisible().selectByVisibleText(brName);
            selectButton.waitUntilVisible().click();
            System.out.println("Branch has changed to : " + brName);
        }
    }

    public void verifyTitle(String pageTitle) {
        if (notificationMessage.isCurrentlyVisible() && notificationMessage.getText().contains("failed")) {
            System.out.println(notificationMessage.getText());
        } else if (getDriver().getTitle().equalsIgnoreCase("Weekly Cash Balancing - error page")) {
            System.out.println(getDriver().getTitle());
        } else {
            if (titleOfPage.getText().equalsIgnoreCase(pageTitle)) {
                System.out.println("User on \"" + titleOfPage.getText() + "\" screen");
            } else {
                System.out.println("User not on \"" + pageTitle + "\" screen");
            }
        }
    }

    public void getBranchName() {
        if (getDriver().getTitle().equalsIgnoreCase("Weekly Cash Balancing - error page")) {
            systemErrorButton.waitUntilVisible().click();
            System.out.println("Current selected branch is : " + branchLink.getText());
        } else
            System.out.println("Current selected branch is : " + branchLink.getText());
    }
}
