package com.johnlewis.weeklyCashBalancing.pageobjects;

import com.johnlewis.weeklyCashBalancing.comman.CreateBranchList;
import com.johnlewis.weeklyCashBalancing.comman.WebdriverHelp;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.*;

public class ViewAuditTrailPage extends PageObject {

    @FindBy(id = "systemLogo")
    WebElementFacade homeButton;

    @FindBy(id = "headerSystemTitle")
    WebElementFacade titleOfPage;

    @FindBy(id = "audit")
    WebElementFacade viewAuditTrail;

    @FindBy(id = "tradingWeek")
    WebElementFacade tradingWeek;

    @FindBy(xpath = "//thead[@class='scrollable-thead']//tr//th")
    List<WebElementFacade> headerTitelList;

    @FindBy(xpath = "//table[@class='scrollable-table']//tbody/tr")
    List<WebElementFacade> headerValueList;

    @FindBy(xpath = "//th[@class='thScrollable']")
    List<WebElementFacade> sortingOptions;

    @FindBy(id = "//button[contains(text(),'Return to Weekly Cash Balancing')]")
    WebElementFacade systemErrorButton;

    WebdriverHelp driverHelp = new WebdriverHelp();
    HomePage homePage = new HomePage();

    public void clickViewAuditTrail() {
        if (titleOfPage.getText().equalsIgnoreCase("Welcome to Weekly Cash Balancing"))
            viewAuditTrail.waitUntilVisible().click();
        else if (getDriver().getTitle().equalsIgnoreCase("Weekly Cash Balancing - error page")) {
            systemErrorButton.waitUntilVisible().click();
            viewAuditTrail.waitUntilVisible().click();
        } else {
            driverHelp.jsClick(getDriver(), homeButton);
            if (driverHelp.isAlertPresent(getDriver()) == true) {
                driverHelp.AlertMessage(getDriver(), "accept");
            }
            viewAuditTrail.waitUntilVisible().click();
        }
    }

    public void getHeaderAuditTrailReport() {
        List<String> headerTitleTextList = new LinkedList<>();
        if (headerTitelList.size() > 0) {
            for (WebElement element : headerTitelList) {
                headerTitleTextList.add(element.getText().replaceAll("\n", " "));
            }
            for (String str : headerTitleTextList) {
                System.out.print("|" + str);
            }
        }
    }

    public void getValueAuditTrailReport() {
        LinkedHashMap<Integer, List<String>> headerValueTextMap = new LinkedHashMap<>();
        if (headerValueList.size() > 0) {
            int i = 1;
            for (WebElement element : headerValueList) {
                WebElement auditDate = element.findElement(By.xpath("td[1]"));
                String date = auditDate.getText().substring(0, 10);
                LinkedList<String> tempValueList = new LinkedList<>();
                List<WebElement> tempValueElementList = element.findElements(By.xpath("td"));
                if (date.equalsIgnoreCase(driverHelp.getRequiredDate("Today's"))) {
                    for (WebElement value : tempValueElementList)
                        tempValueList.add(value.getText().replaceAll("\n", " "));
                    i++;
                    headerValueTextMap.put(i, tempValueList);
                }
            }
            for (Map.Entry<Integer, List<String>> value : headerValueTextMap.entrySet()) {
                for (String val : value.getValue())
                    System.out.print("|" + val.replaceAll("\n", " "));
                System.out.println();
            }

        }
    }

    public void printAllBranchAuditTrailReport() {
        CreateBranchList branch = new CreateBranchList();
        LinkedHashSet<String> branchList = branch.readBranchList();
        for (String str : branchList) {
            homePage.selectBranch(str);
            clickViewAuditTrail();
            getHeaderAuditTrailReport();
            getValueAuditTrailReport();
        }
    }

    public void displayAuditTrail() {
        String tradingWeekValue = tradingWeek.getSelectedVisibleTextValue().trim();
        System.out.println(tradingWeekValue + " trading week is selected");

        for (int j = 0; j < sortingOptions.size(); j++) {
            System.out.println("Audit trail is sorted by \"" + sortingOptions.get(j).getText().replaceAll("\n?","") + "\"");
            System.out.println("==========================================================");
            sortingOptions.get(j).waitUntilVisible().click();
            List<String> headerTitleTextList = new LinkedList<>();
            LinkedHashMap<Integer, List<String>> headerValueTextMap = new LinkedHashMap<>();

            for (WebElement element : headerTitelList) {
                headerTitleTextList.add(element.getText().replaceAll("\n?", " "));
            }
            int i = 1;
            for (WebElement element : headerValueList) {
                WebElement auditDate = element.findElement(By.xpath("td[1]"));
                String date = auditDate.getText().substring(0, 10);
                LinkedList<String> tempValueList = new LinkedList<>();
                List<WebElement> tempValueElementList = element.findElements(By.xpath("td"));
                if (date.equalsIgnoreCase(driverHelp.getRequiredDate("Today's"))) {
                    for (WebElement value : tempValueElementList)
                        tempValueList.add(value.getText().replaceAll("\n", " "));
                    i++;
                    headerValueTextMap.put(i, tempValueList);
                }

            }
            for (String str : headerTitleTextList) {
                System.out.print("|" + str);
            }
            System.out.println();
            for (Map.Entry<Integer, List<String>> value : headerValueTextMap.entrySet()) {
                for (String val : value.getValue())
                    System.out.print("|" + val);
                System.out.println();
            }
            System.out.println();
        }
    }


}
