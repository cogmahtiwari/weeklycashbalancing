#Going log off from Weekly Cash Balancing Application

Feature: Log off from Weekly Cash Balancing Application

  Scenario Outline: To verify that user is able to log off from weekly cash balancing application
    Given User on homepage of weekly cash balancing and changes branch to "Peter Jones"
    Then verify the title of page is "Welcome to Weekly Cash Balancing"
    When user click on help link verify the title of page is "Weekly Cash Balancing Help"
    And  user press CLTR and click on to open the menu in multiple tab and Verify tital of page "<title of page>"
    Then user logoff from WCB Application
    Examples:
      | title of page                                                                                                                                     |
      | Enter Daily WCB Totals,Enter Cash Movements,View and Submit Weekly Totals,Maintain Registers and Sections,View Audit Trail,Submit Progress Tracker |

