#Login into Weekly Cash Balancing Application

Feature: Login into Weekly Cash Balancing Application

  Scenario Outline: To verify that user is able to login into weekly cash balancing application
    Given User opens the WCB Application url to login
    Then verify the title of page is "Log on to Weekly Cash Balancing"
    When User login to WCB Application using  valid userName "<username>" and password "<password>" and click logon button
    Then verify the title of page is "Select Branch"
    When User able to select "<branch>"
    Then verify the title of page is "Welcome to Weekly Cash Balancing"
    Examples:
      | username | password | branch        |
      #| T12345   | cogni1   |               |
      | TPU201   | sep2019  | Oxford Street |

  Scenario Outline: To verify that close float and reserve close are available for valid days only
    Given user on the Enter Daily WCB totals Screen
    Then verify the title of page is "Enter Daily WCB Totals"
    When user enter the <day> as date
    Then date out of range message should be displayed
    When user select a date that is a "saturday"
    Then closing Float and closing reserve columns are editable
    When user change the date with saving details to that is not a saturday
    Then closing float and closing reserve columns are protected and warning message should display
    When change the branch after entries have been keyed
    Then warning message should display and return to homepage
    When user click on Home before saving changes
    Then warning message should display
    Examples:
      | day            |
      | today's,future |

  Scenario: To verify that value enter at section and register level are as per business rule
    Given user on the Enter Daily WCB totals Screen
    Then verify the title of page is "Enter Daily WCB Totals"
    When user select section to enter the floating details then assigned register should display
    And  user enter float at section level then register level fields get protected
    And  user enter float at register level as as per business rule
    And  section level fields get protected and user allowed to enter value as per business rule
    Then cancel the user input
    When user select a date from a week that has been submitted already
    Then There should be no editable fields & a message of 'Totals Submitted'
    When user click on section S00 and S99 then S00 register only display
    And  Verify Movement details are not editable
#    And  user enter value in adjustment for section S00
#    Then for section S99 Field should be protected
#    Then for section S00,S99,SAFE float and change machine Field should be protected
    Then for section S00 and S99 Field should be protected

  Scenario Outline: To verify the Enter Cash Movements Screen
    Given User able to open Enter Cash Movements
    Then  verify the title of page is "Enter Cash Movements"
    When user enter the <date> as cash movement trading date
    Then date out of range message should be displayed for future <date> only
    And user enter cash movement details, user allowed to enter value as per business rule
    When user select a date from a week that has been submitted already
    And There should be no editable fields & a message of 'This Week Totals Submitted'
    Then  user do <activity> after entries have been keyed for cash movement then Warning message should display

    Examples:
      | date           | activity                                                              |
      | today's,future | Change Date,Change Branch,Cancel Movement,Click HomeButton,Save Blank |

  Scenario: To verify Maintain Register and section screen
    Given user on Maintain Registers and Sections screen
    Then verify the title of page is "Maintain Registers and Sections"
    When user sort section by name and code then section display accordingly
    And  user click on sections then available section should display
    Then user click on edit details then register and section validation should be as per business rule

  Scenario Outline: Add section to Maintain Register and section screen
    Given user on Maintain Registers and Sections screen
    Then verify the title of page is "Maintain Registers and Sections"
    When user add section "<Section>" with description "<Description>"
    Then details should be added
    Examples:
      | Section | Description              |
      #| S98     | New section-Test         |
      | S01     | Duplicat section         |
      | S43     |                          |
      |         | No section code          |
      | S43     | WOMENSWEAR NORTH 01 BANK |
      | 112     | Not S section            |

  Scenario Outline: Add register to Maintain Register and section screen
    Given user on Maintain Registers and Sections screen
    Then verify the title of page is "Maintain Registers and Sections"
    When user add register "<Register>" with description "<Description>" in section "<Section>"
    Then details should be added
    Examples:
      | Section | Register | Description        |
      | S01     | 104      | New register       |
      | S01     | 436      | Duplicate register |
      | S01     | 102      |                    |
      | S01     | 103      | New register       |
      |         | 222      | Unassigned section |

  Scenario Outline: To verify View and Submit Weekly Totals screen
    Given user on the View and Submit Weekly Totals
    Then verify the title of page is "View and Submit Weekly Totals"
    When user select the <week> as trading week and details should display
    Then Change to another authorised branch before and after submitting
    When user sort section by name and code list should display accordingly
    Then user submit for week then warning message should display before submitting
    Examples:
      | week                |
      | current,future,past |

  Scenario Outline: To verify the Submit Progress Tracker screen
    Given user able to open Submit Progress Tracker
    Then verify the title of page is "Submit Progress Tracker"
    When user select the <week> as trading week and tracker details should display
    And  user change to another authorised branch to "Knight and Lee"
    And  user click on pass back to resubmit weekly totals
    Then pass back only enabled for branches that have submitted their week
    Examples:
      | week         |
      | current,past |

  Scenario: To verify the View Audit Trail Screen
    Given User able to open View Audit Trail
    Then  verify the title of page is "View Audit Trail"
    When  user select the week as trading week and sorted audit details should display
    And  user change to another authorised branch to "Basingstoke (at home)"
    Then  display the audit report of all branch
