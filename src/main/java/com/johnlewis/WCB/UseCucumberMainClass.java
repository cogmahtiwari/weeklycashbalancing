package com.johnlewis.WCB;

import cucumber.api.cli.Main;
import org.junit.runner.JUnitCore;

public class UseCucumberMainClass {
    public static void main(String[] args) throws Throwable {
        /*Main.main(new String[]
                {"-g", "src/test/java/com/johnlewis/weeklyCashBalancing/stepdefs/WCBStepDefination.java","src/test/resources/features/LoginWCB.feature"});*/

        Main.main(new String[]{"--glue", "com/johnlewis/weeklyCashBalancing/stepdefs", "src/test/resources/features"});
    }
}
